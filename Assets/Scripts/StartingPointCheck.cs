﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingPointCheck : MonoBehaviour {
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Alien"))
		{
			Destroy(other.gameObject);
		}
	}
}
