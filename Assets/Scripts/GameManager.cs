﻿//This script manages the timing and flow of the game. It is also responsible for telling
//the UI when and how to update

using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using ControlFreak2;
using SWS;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
	//The game manager holds a public static reference to itself. This is often referred to
	//as being a "singleton" and allows it to be access from all other objects in the scene.
	//This should be used carefully and is generally reserved for "manager" type objects
	public static GameManager instance;
	[Header("Race Settings")]
	public int numberOfLaps = 3;			//The number of laps to complete
	public VehicleMovement vehicleMovement;	//A reference to the ship's VehicleMovement script
	[Header("UI References")]
	public ShipUI shipUI;					//A reference to the ship's ShipUI script
	public LapTimeUI lapTimeUI;				//A reference to the LapTimeUI script in the scene
	public GameObject gameOverUI;			//A reference to the UI objects that appears when the game is complete
	float[] lapTimes;						//An array containing the player's lap times
	[SerializeField]
	public int currentLap;						//The current lap the player is on
	public bool isGameOver;						//A flag to determine if the game is over
	bool raceHasBegun;

	public int CurrentLevel;

	//A flag to determine if the race has begun

	#region Aman Variables
	public GameObject touchControl;
	public enum MyControl
	{
		touch,acce
	}
	public Canvas controlFreak;
	//public Texture[] levelPlanet;
	public RawImage textureChange;
	public Canvas lapUI;
	public GameObject minimap;
	public GameObject pauseAlert;
	public GameObject beforeGameStart;
	public GameObject warpSystem;
	// all ships in array to on off
	public GameObject[] allShips;
	//provide camera to spaceship
	public CinemachineVirtualCamera virtualCam;
	[SerializeField]
	private CinemachineTransposer cineTrans;
	//define delegate for Pause alert buttons
	public delegate void PauseAlertButtonManage(int i);
	//define event for Pause Alert Manager
	public static event PauseAlertButtonManage pauseAlertButtonManage;
	//for third perso static camera position
	public Vector3 thirdPerson;
	//for first perso static camera position
	public Vector3 first;
	private bool eyeChange;
	private bool controlChange;
	public MyControl controls;
	public Sprite[] controlIcon;
	public GameObject controlIconChange;
	public List<Vector3> angleValue = new List<Vector3>();
	public GameObject ShieldObj;
	public GameObject NitroObj;
	public Image sliderShield;
	public Image NitroSlider;
	private bool shieldBool;
	private bool nitroBool;
	private float countTime;
	private float countTime1;
	public Texture[] OutOfRank_Crash;
	public GameObject crash_out_title;
	public GameObject scoreBoard;
	public GameObject crystal_diamond;
	public TextMeshProUGUI shieldRemaining;
	public TextMeshProUGUI nitroRemaining;
	public TextMeshProUGUI[] allRewardValues;
	#endregion

	void Awake()
	{
		Time.timeScale = 1f;
		PlayerPrefs.SetString("CurrentScene", SceneManager.GetActiveScene().name);
		warpSystem.SetActive(false);
		//it will be true in starting
		shieldBool = true;
		nitroBool = true;

		for (int i = 0; i < allShips.Length; i++)
		{

			if (allShips[i].name == PlayerPrefs.GetString("SelectedShip"))
			{
				allShips[i].SetActive(true);
				vehicleMovement = allShips[i].GetComponent<VehicleMovement>();
				shipUI = allShips[i].GetComponent<PlayerInput>().shipUI;
				virtualCam.LookAt = allShips[i].transform;
				virtualCam.Follow = allShips[i].transform;
				PlayerPrefs.SetInt("SelectShipInt", i);
			}
		}

		if (SceneManager.GetActiveScene().name == "Juipter1")
		{

		}
		//If the variable instance has not be initialized, set it equal to this
		//GameManager script...
		if (instance == null)
			instance = this;
		//...Otherwise, if there already is a GameManager and it isn't this, destroy this
		//(there can only be one GameManager)
		else if (instance != this)
			Destroy(gameObject);

		//Disable GameObject in starting game
		DisableInStart(false);

	//Debug.Log(PlayerPrefs.GetString("Level"));
		//get selected planet name from playerPrefrence
		PlanetDetail(PlayerPrefs.GetString("Level"));
		eyeChange = true;
		cineTrans = FindObjectOfType<CinemachineTransposer>();
		checkWhichObjectIsTrue();
	}

	private void Start()
	{
		/*crystal_diamond.transform.GetChild(0).
			transform.GetChild(0).GetComponent<TextMeshPro>().text =  PlayerPrefs.GetInt("useCrystal").ToString();

		crystal_diamond.transform.GetChild(1).
			transform.GetChild(0).GetComponent<TextMeshPro>().text =  PlayerPrefs.GetInt("useDiamond").ToString();*/

		shieldRemaining.text = PlayerPrefs.GetInt("useShield").ToString();
		nitroRemaining.text = PlayerPrefs.GetInt("useNitro").ToString();
		RewardValueSet();
	}

	private void RewardValueSet()
	{
		GameObject manager = PlayAreaSoundManager.Instance.soundManager;

		if (SceneManager.GetActiveScene().name.Contains("1"))
		{
			allRewardValues[0].text = manager.GetComponent<SoundManager>().RewardData[3].ToString();
			allRewardValues[1].text = manager.GetComponent<SoundManager>().RewardData[4].ToString();
			allRewardValues[2].text = manager.GetComponent<SoundManager>().RewardData[5].ToString();
		}
		else
		{
			allRewardValues[0].text = manager.GetComponent<SoundManager>().RewardData[0].ToString();
			allRewardValues[1].text = manager.GetComponent<SoundManager>().RewardData[1].ToString();
			allRewardValues[2].text = manager.GetComponent<SoundManager>().RewardData[2].ToString();
		}
	}

	public void Shield()
	{
		int shield = PlayerPrefs.GetInt("useShield");

		if (shield <= 0)
		{
			Debug.Log("NO eNOUGH sHIELD");
		}
		else
		{
			PlayerPrefs.SetInt("useShield", PlayerPrefs.GetInt("useShield") - 1);
			shieldBool = true;
			vehicleMovement.Shield();
			ShiedManage(false);
			StartCoroutine(ShieldOn());
		}

		RemainingNitroShield();
	}

	public void RemainingNitroShield()
	{
		shieldRemaining.text = PlayerPrefs.GetInt("useShield").ToString();
		nitroRemaining.text = PlayerPrefs.GetInt("useNitro").ToString();
	}

	public void Nitro()
	{
//			Debug.Log("Nitro");
			int nitro = PlayerPrefs.GetInt("useNitro");


			if (nitro <= 0)
			{
				Debug.Log("NO eNOUGH nITRO");
			}
		else
		{
			PlayerPrefs.SetInt("useNitro", PlayerPrefs.GetInt("useNitro") - 1);
			nitroBool = true;
			vehicleMovement.gameObject.GetComponent<VehicleAVFX>().currentLifeTime = 0.3f;
			vehicleMovement.Nitro();
			NitroManage(false);
			StartCoroutine(NitroOn());
		}
		RemainingNitroShield();
	}

	private void ShiedManage(bool b)
	{
		ShieldObj.GetComponent<Button>().interactable = b;
		ShieldObj.transform.GetChild(1).GetComponent<RawImage>().enabled = b;
	}


	private void NitroManage(bool b)
	{
		NitroObj.GetComponent<Button>().interactable = b;
		NitroObj.transform.GetChild(1).GetComponent<RawImage>().enabled = b;
	}

	IEnumerator NitroOn()
	{
		if (nitroBool)
		{
			countTime += 0.1f ;
			NitroSlider.fillAmount = countTime / 10;
			yield return new WaitForSeconds(0.1f);

			if (countTime >= 10)
			{
				NitroManage(true);
				vehicleMovement.gameObject.GetComponent<VehicleAVFX>().currentLifeTime = 0.1f;
				NitroSlider.fillAmount = 0f;
				countTime = 0;
				nitroBool = false;
				StopCoroutine(NitroOn());
			}
			else
			{
				StartCoroutine("NitroOn");
			}
		}

	}

	IEnumerator ShieldOn()
	{

		if (shieldBool)
		{
			countTime1 += 0.1f;
			sliderShield.fillAmount =  countTime1 / 10;
			yield return new WaitForSeconds(0.1f);

			if (countTime1 >= 10)
			{
				ShiedManage(true);
				sliderShield.fillAmount = 0f;
				countTime1 = 0;
				shieldBool = false;
				StopCoroutine(ShieldOn());
			}
			else
			{
				StartCoroutine("ShieldOn");
			}
		}

	}

	public void CurrentHealth()
	{
		int totalProgress = (int) (
			(double) vehicleMovement.health / vehicleMovement.HealthStatic * 100);
				PlayerPrefs.SetInt("Damage"+vehicleMovement.gameObject.name, totalProgress);
				PlayerPrefs.SetInt("ShipHealth",totalProgress);
	}


	private void checkWhichObjectIsTrue()
	{
		if (allShips[0].activeSelf)
		{
			thirdPerson = angleValue[0];
			first = angleValue[1];
		}
		else if (allShips[1].activeSelf)
		{
			thirdPerson = angleValue[2];
			first = angleValue[3];
		}
		else if (allShips[2].activeSelf)
		{
			thirdPerson = angleValue[4];
			first = angleValue[5];
		}
		else if (allShips[3].activeSelf)
		{
			thirdPerson = angleValue[6];
			first = angleValue[7];
		}

		//default position will be 3rd person
		cineTrans.m_FollowOffset = thirdPerson;
	}

	private void DisableInStart(bool value)
	{
		// in start we will disable control freak
		controlFreak.enabled = value;
		//lap UI WILL BE DISABLE
		lapUI.enabled = value;
		//minimap will be disable first
		minimap.SetActive(value);

	}

	private void PlanetDetail(string getString)
	{
		beforeGameStart.SetActive(true);
	/*	if (getString.Contains("Marcury"))
		{
			textureChange.texture = levelPlanet[0];
		}
		else if (getString.Contains("Venus"))
		{
			textureChange.texture = levelPlanet[1];
		}
		else if (getString.Contains("Earth"))
		{
			textureChange.texture = levelPlanet[2];
		}
		else if (getString.Contains("Mars"))
		{
			textureChange.texture = levelPlanet[3];
		}
		else if (getString.Contains("Jupiter"))
		{
			textureChange.texture = levelPlanet[4];
		}*/
	}


	public void EyeChange()
	{
		if (eyeChange)
		{
			cineTrans.m_FollowOffset = first;
			eyeChange = false;
		}
		else
		{
			cineTrans.m_FollowOffset = thirdPerson;
			eyeChange = true;
		}
	}

	public void ControlChange()
	{
		if (controlChange)
		{
			touchControl.SetActive(false);
			controls = MyControl.acce;
			controlChange = false;
			controlIconChange.GetComponent<Image>().sprite = controlIcon[0];
		}
		else
		{
			touchControl.SetActive(true);
			controls = MyControl.touch;
			controlChange = true;
			controlIconChange.GetComponent<Image>().sprite = controlIcon[1];
		}
	}



	private void FixedUpdate()
	{

		if (ControlFreak2.CF2Input.GetButtonDown("Pause"))
		{
			Time.timeScale = 0f;
			pauseAlert.SetActive(true);
			DisableInStart(false);
		}

/*		if (ControlFreak2.CF2Input.GetButtonDown("Eye"))
		{
			if (eyeChange)
			{
				cineTrans.m_FollowOffset = first;
				eyeChange = false;
			}
			else
			{
				cineTrans.m_FollowOffset = thirdPerson;
				eyeChange = true;
			}
		}*/

		if (ControlFreak2.CF2Input.GetButtonDown("Control"))
		{

		}

	}

	void OnEnable()
	{
		//When the GameManager is enabled, we start a coroutine to handle the setup of
		//the game. It is done this way to allow our intro cutscene to work. By slightly
		//delaying the start of the race, we give the cutscene time to take control and
		//play out
		StartCoroutine(Init());
		pauseAlertButtonManage += PauseAlertManager;

	}

	private void OnDisable()
	{
		pauseAlertButtonManage -= PauseAlertManager;
	}

	public void PauseAlertButton(int value)
	{
		PauseAlertManager(value);
	}

	private void PauseAlertManager(int value)
	{
		if (value.Equals(0))
		{
			Home();
		}
		else if (value.Equals(1))
		{
			Time.timeScale = 1f;
			pauseAlert.SetActive(false);
			DisableInStart(true);
		}
		else if (value.Equals(2))
		{
			PlayerPrefs.SetInt("Home", 1);
		//	SoundManager.Instance.PlayAreaBack.mute = true;
			SoundManager.Instance.BTSound(4);
			SceneManager.LoadScene("loading");
		}
		else if (value.Equals(3))
		{
			for (int i = 0; i < GetComponent<RaceManager>().textRanks.Length; i++)
			{
				if (GetComponent<RaceManager>().textRanks[i].text.Contains("Ship"))
				{
					int RewardDiamond = int.Parse(GetComponent<RaceManager>().rewards[i].text);
					PlayerPrefs.SetInt("PlayAreaDiamond", PlayerPrefs.GetInt("PlayAreaDiamond") + RewardDiamond);
					int rank = i + 1;

					//Debug.Log("Current : " + getPositionCalculate(CurrentLevel) + "Rank :" +  rank);

					PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 10);

					if (getPositionCalculate(CurrentLevel) == 0)
					{
						setPositionCalculate(CurrentLevel,rank);
					}
					else if(rank < getPositionCalculate(CurrentLevel))
					{
						setPositionCalculate(CurrentLevel,rank);
					}
				}
			}
			Home();
			PlayerPrefs.SetInt("AfterRaceFinish",10);
		}

	}

	public static void setPositionCalculate (int lvlNo, int position)
	{
		PlayerPrefs.SetInt ("levelNo" + lvlNo + "Position", position);
	}

	public static int getPositionCalculate (int lvlNo)
	{
		return PlayerPrefs.GetInt ("levelNo" + lvlNo + "Position");
	}

	void Home()
	{
		Time.timeScale = 1f;
	//	SoundManager.Instance.PlayAreaBack.mute = true;
		PlayerPrefs.SetInt("Home", 0);
		SoundManager.Instance.BTSound(4);
		SceneManager.LoadScene("loading");
	}

	IEnumerator Init()
	{
		//Update the lap number on the ship
		UpdateUI_LapNumber();

		//Wait a little while to let everything initialize
		yield return new WaitForSeconds(.1f);

		//Initialize the lapTimes array and set that the race has begun
		lapTimes = new float[numberOfLaps];
		raceHasBegun = true;
	}

	void Update()
	{
//		Debug.Log(isGameOver);
		//Update the speed on the ship UI
		UpdateUI_Speed ();

		//If we have an active game...
		if (IsActiveGame())
		{
			//...calculate the time for the lap and update the UI
			lapTimes[currentLap] += Time.deltaTime;
			UpdateUI_LapTime();
		}
	}

	//Called by the FinishLine script
	public void PlayerCompletedLap()
	{
		//If the game is already over exit this method
		if (isGameOver)
			return;

		//Incrememebt the current lap
		currentLap++;

		//Update the lap number UI on the ship
		UpdateUI_LapNumber ();

		//If the player has completed the required amount of laps...
		if (currentLap >= numberOfLaps)
		{
			//...the game is now over...
			isGameOver = true;
			//...update the laptime UI...
			UpdateUI_FinalTime();
			//...and show the Game Over UI
			vehicleMovement.GetComponent<splineMove>().enabled = true;
			vehicleMovement.enabled = false;
			vehicleMovement.GetComponent<PlayerInput>().enabled = false;
			vehicleMovement.GetComponent<VehicleAVFX>().enabled = false;
			Destroy(vehicleMovement.GetComponent<Rigidbody>());
			controlFreak.enabled = false;
			minimap.SetActive(false);
			SetScoreBoard();
		}
	}

	public void SetScoreBoard()
	{
		scoreBoard.SetActive(true);
		gameOverUI.SetActive(true);
		scoreBoard.transform.GetChild(1).gameObject.SetActive(false);
		StartCoroutine("ScoreBoardShow");
	}

	public void Venus1Eliminate()
	{
		//...the game is now over...
		isGameOver = true;
		//...update the laptime UI...
		UpdateUI_FinalTime();
		//...and show the Game Over UI
		vehicleMovement.GetComponent<splineMove>().enabled = true;
		vehicleMovement.enabled = false;
		vehicleMovement.GetComponent<PlayerInput>().enabled = false;
		vehicleMovement.GetComponent<VehicleAVFX>().enabled = false;
		Destroy(vehicleMovement.GetComponent<Rigidbody>());
		controlFreak.enabled = false;
		minimap.SetActive(false);
		scoreBoard.SetActive(true);
		gameOverUI.SetActive(true);
		scoreBoard.transform.GetChild(1).gameObject.SetActive(false);
		StartCoroutine("ScoreBoardShow");
	}


	public void RunOutOfTime()
	{
		Time.timeScale = 0f;
		crash_out_title.GetComponent<RawImage>().transform.parent.gameObject.SetActive(true);
		crash_out_title.GetComponent<RawImage>().texture =
		OutOfRank_Crash[2];

	//	crash_out_title.gameObject.transform.parent.transform.GetChild(4).gameObject.SetActive(false);
		crash_out_title.gameObject.transform.parent.transform.GetChild(3).gameObject.SetActive(true);
		controlFreak.enabled = false;

		if (vehicleMovement != null)
		{
			Destroy(vehicleMovement.gameObject);
		}
	}


	IEnumerator ScoreBoardShow()
	{
		yield return new WaitForSeconds(3f);
		scoreBoard.transform.GetChild(1).gameObject.SetActive(true);
		gameOverUI.SetActive(false);
	}

	void UpdateUI_LapTime()
	{
		//If we have a LapTimeUI reference, update it
		if (lapTimeUI != null)
			lapTimeUI.SetLapTime(currentLap, lapTimes[currentLap]);
	}

	void UpdateUI_FinalTime()
	{
		//If we have a LapTimeUI reference...
		if (lapTimeUI != null)
		{
			float total = 0f;

			//...loop through all of the lapTimes and total up an amount...
			for (int i = 0; i < lapTimes.Length; i++)
				total += lapTimes[i];

			//... and update the final race time
			lapTimeUI.SetFinalTime(total);
		}
	}

	void UpdateUI_LapNumber()
	{
		//If we have a ShipUI reference, update it
		if (shipUI != null)
			shipUI.SetLapDisplay (currentLap + 1, numberOfLaps);
	}

	void UpdateUI_Speed()
	{
		//If we have a VehicleMovement and ShipUI reference, update it
		if (vehicleMovement != null && shipUI != null)
		{
			shipUI.SetSpeedDisplay (Mathf.Abs(vehicleMovement.speed));
			if (Mathf.Abs(vehicleMovement.speed) > 50f)
			{
				warpSystem.SetActive(true);
			}
			else
			{
				warpSystem.SetActive(false);
			}
		}

	}

	public bool IsActiveGame()
	{
		//If the race has begun and the game is not over, we have an active game
		return raceHasBegun && !isGameOver;
	}



}
