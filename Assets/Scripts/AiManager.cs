﻿using System.Collections;
using System.Collections.Generic;
using SWS;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AiManager : MonoBehaviour {

//	public GameObject EnemyAi;
	public GameObject crystal, Daimond, Nitro,shield, enemy1,enemy2;
	public PathManager spawnManager;

	//public int maxEnemyAi = 15;
	public int maxSpawing;
	public int NitoShield;
	public int AI;
	public int fromPointToSpawn;
	public float spawnDistance;

	public TextMeshProUGUI crystalText, DiamondText;

	public static AiManager Instance = null;

	//public GameObject fireBt, shieldBt;

	// Use this for initialization
	void Start () {

		if (Instance == null)
		{
			Instance = this;
		}

		/*for (int i = 0; i < maxEnemyAi; i++)
		{

			int randomValue = Random.Range(0,spawnManager[0].waypoints.Length);
			Instantiate(EnemyAi, spawnManager[0].waypoints[randomValue].transform.position,
				spawnManager[0].waypoints[randomValue].transform.rotation);
		}*/

		Diamond();
		crystalSet();
		NitroSet();
		ShieldSet();
		Enemy1Set();
		Enemy2Set();

		crystalText.text = "000";
		DiamondText.text = "00";

	}

	private void Enemy2Set()
	{
		for (int i = 0; i < AI; i++)
		{
			int randomCoin = Random.Range(fromPointToSpawn,spawnManager.waypoints.Length - 4);
			float randomRange = Random.Range(spawnManager.waypoints[randomCoin].transform.position.z + spawnDistance
				, spawnManager.waypoints[randomCoin].transform.position.z - spawnDistance);

			Instantiate(enemy2, new Vector3(spawnManager.waypoints[randomCoin].transform.position.x,
				spawnManager.waypoints[randomCoin].transform.position.y, randomRange), shield.transform.localRotation);
		}
	}

	private void Enemy1Set()
	{
		for (int i = 0; i < AI; i++)
		{
			int randomCoin = Random.Range(fromPointToSpawn,spawnManager.waypoints.Length);
			float randomRange = Random.Range(spawnManager.waypoints[randomCoin].transform.position.z + spawnDistance
				, spawnManager.waypoints[randomCoin].transform.position.z - spawnDistance);

			Instantiate(enemy1, new Vector3(spawnManager.waypoints[randomCoin].transform.position.x,
				spawnManager.waypoints[randomCoin].transform.position.y, randomRange), shield.transform.localRotation);
		}
	}

	private void ShieldSet()
	{
		for (int i = 0; i < NitoShield; i++)
		{
			int randomCoin = Random.Range(fromPointToSpawn,spawnManager.waypoints.Length);
			float randomRange = Random.Range(spawnManager.waypoints[randomCoin].transform.position.z + spawnDistance
				, spawnManager.waypoints[randomCoin].transform.position.z - spawnDistance);

			Instantiate(shield, new Vector3(spawnManager.waypoints[randomCoin].transform.position.x,
				spawnManager.waypoints[randomCoin].transform.position.y, randomRange), shield.transform.localRotation);
		}
	}

	private void NitroSet()
	{
		for (int i = 0; i < NitoShield; i++)
		{
			int randomCoin = Random.Range(fromPointToSpawn, spawnManager.waypoints.Length);
			float randomRange = Random.Range(spawnManager.waypoints[randomCoin].transform.position.z + spawnDistance
				, spawnManager.waypoints[randomCoin].transform.position.z - spawnDistance);

			Instantiate(Nitro, new Vector3(spawnManager.waypoints[randomCoin].transform.position.x,
				spawnManager.waypoints[randomCoin].transform.position.y, randomRange), Nitro.transform.localRotation);
		}
	}

	private void Diamond()
	{
		for (int i = 0; i < maxSpawing; i++)
		{
			int randomCoin = Random.Range(fromPointToSpawn,spawnManager.waypoints.Length);
			float randomRange = Random.Range(spawnManager.waypoints[randomCoin].transform.position.z + spawnDistance
				, spawnManager.waypoints[randomCoin].transform.position.z - spawnDistance);

			Instantiate(Daimond, new Vector3(spawnManager.waypoints[randomCoin].transform.position.x,
				spawnManager.waypoints[randomCoin].transform.position.y, randomRange), Daimond.transform.localRotation);
		}
	}

	private void crystalSet()
	{
		for (int j = 0; j < maxSpawing; j++)
		{
			int randomCrystal = Random.Range(fromPointToSpawn,spawnManager.waypoints.Length);
			float randomRange = Random.Range(spawnManager.waypoints[randomCrystal].transform.position.z + spawnDistance
				, spawnManager.waypoints[randomCrystal].transform.position.z - spawnDistance);

			Instantiate(crystal, new Vector3(spawnManager.waypoints[randomCrystal].transform.position.x,
				spawnManager.waypoints[randomCrystal].transform.position.y, randomRange), crystal.transform.localRotation);
		}
	}

	public void UpdateValues()
	{
		crystalText.text = PlayerPrefs.GetInt("PlayAreaCrystal").ToString();
		DiamondText.text = PlayerPrefs.GetInt("PlayAreaDiamond").ToString();
	}

}
