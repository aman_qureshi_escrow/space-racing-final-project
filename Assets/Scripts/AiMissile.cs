﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiMissile : MonoBehaviour
{

	private Rigidbody rb;
	
	
	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		StartCoroutine("DestroyMissile");
	}



	IEnumerator DestroyMissile()
	{
		yield return new WaitForSeconds(3f);
		Destroy(this.transform.parent.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		rb.AddForce(transform.forward);
	}

	private void OnTriggerEnter(Collider other)
	{
		Destroy(this.transform.parent.gameObject);
	}
}
