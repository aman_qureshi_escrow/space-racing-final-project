﻿using UnityEngine;
using System.Collections;

public class EnemyAlien : MonoBehaviour {

	private float deadLength = 1.0f;
	private float explotionLength = 1.0f;
	public GameObject explotionPrefab;
	public Transform firePoint;
	public GameObject missile;
	public float timeDelayForShoot;
	private float timeStamp;

	//Enemy Health
	private float health;

	//Enemy AI
	private float speed = 15.0f;
	public Transform player;
	private float followDistance = 100.0f;
	private float minimumDistance = 90f;
	private Vector3 enemyToPlayerDistance;
	private float lockYPosition = -5.5f; //Player is floating we don't want enemies to float so lets use this to adjust it

	private bool canAttack = false;

	//particle
	public GameObject DeadParticle;

	void Start(){
		health = 2.0f;
		player = GameObject.FindWithTag("Player").transform;
		StartCoroutine("GetPermisssionToAttack");
	}

	IEnumerator GetPermisssionToAttack()
	{
		yield return new WaitUntil(()=> player.GetComponent<VehicleMovement>().timerText.text.Equals("Go!"));
		canAttack = true;
	}

	void Update(){

		if (canAttack == true)
		{
			if(health == 0 || player == null)
				return;

			enemyToPlayerDistance = player.transform.position - transform.position;
			//Debug.Log(enemyToPlayerDistance.ToString());

			transform.LookAt(player);

			var dist = enemyToPlayerDistance.magnitude;

			//Debug.Log("Magnitude: " + dist.ToString());

			if(dist <= followDistance) {
				//	Debug.Log("Following Player");

				if (dist <= minimumDistance)
				{
					Attack();
				}
				else
				{
					// RunTowardPlayer();
				}

			}
			else {
				//Idle();
			}
		}

	}

	private void Attack()
	{
//		Debug.Log("Attack");
		firePoint.LookAt(player);

		if (Time.time >= timeStamp)
		{
//			Debug.Log("Attack");
			SoundManager.Instance.SPSound(0);
			//GetComponent<Animation>().Play("byte");
			Instantiate(missile, firePoint.position, firePoint.rotation);
			timeStamp = Time.time + timeDelayForShoot;
		}


	}

	void RunTowardPlayer(){

		enemyToPlayerDistance.y = lockYPosition;
		transform.position = Vector3.MoveTowards(transform.position, enemyToPlayerDistance, speed * Time.deltaTime);
	//	controller.Move(enemyToPlayerDistance.normalized * speed * Time.deltaTime );

		//if(!GetComponent<Animation>().isPlaying)
    	//	GetComponent<Animation>().CrossFade("walk", 0.3f);
	}
	void Idle(){
	//	GetComponent<Animation>().CrossFade("walk", 0.3f);
	}
	void OnCollisionEnter(Collision collision){

		Debug.Log(collision.gameObject.name);

		if(collision.gameObject.tag == "Player" && health > 0){
//			Debug.Log("Attack...");
		//	GetComponent<Animation>().CrossFade("byte", 0.5f);
		}
		else if(collision.gameObject.tag == "SpaceshipBullet" && health > 0){
			health--;
			if(health == 0){
			//	iTween.ShakePosition(gameObject,new Vector3(1.0f,1.0f,1.0f),1.0f);
				GameObject explotion =
					Instantiate(explotionPrefab,gameObject.transform.position, Quaternion.identity);

				//Bonus for destroying Defense Enemy
			//	Globals.score += Globals.bonusPoints;
			//	Globals.inProgressScoreBonus = true; //Show + bonus message
			//	Utils.UpdateHighScore(Globals.score);

				//GetComponent<Animation>().Play("walk");
				Destroy(gameObject,deadLength);
				Destroy(explotion,explotionLength);
			}
			else {
				GetComponent<Animation>().Play("byte");
			}
		}

		if (collision.gameObject.CompareTag("PlayerMissile"))
		{
			SoundManager.Instance.SPSound(1);
			Instantiate(DeadParticle, transform.position, transform.rotation);
			Destroy(this.gameObject);
		}

	}

	private void OnTriggerEnter(Collider collision)
	{

////		Debug.Log(collision.gameObject.name);

		if(collision.gameObject.tag == "Player" && health > 0){
//			Debug.Log("Attack...");
			//	GetComponent<Animation>().CrossFade("byte", 0.5f);
		}
		else if(collision.gameObject.tag == "PlayerMissile" && health > 0){
			health--;
			if(health == 0){
				//	iTween.ShakePosition(gameObject,new Vector3(1.0f,1.0f,1.0f),1.0f);
				GameObject explotion =
					Instantiate(explotionPrefab,gameObject.transform.position, Quaternion.identity);

				//Bonus for destroying Defense Enemy
				//	Globals.score += Globals.bonusPoints;
				//	Globals.inProgressScoreBonus = true; //Show + bonus message
				//	Utils.UpdateHighScore(Globals.score);

				//GetComponent<Animation>().Play("walk");
				Destroy(gameObject,deadLength);
				Destroy(explotion,explotionLength);
			}
			else {
			//	GetComponent<Animation>().Play("byte");
			}
		}

		if (collision.gameObject.CompareTag("PlayerMissile"))
		{
			SoundManager.Instance.SPSound(1);
			Instantiate(DeadParticle, transform.position, transform.rotation);
			Destroy(this.gameObject);
		}

	}
}
