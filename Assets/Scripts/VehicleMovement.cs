﻿//This script handles all of the physics behaviors for the player's ship. The primary functions
//are handling the hovering and thrust calculations.

using System;
using System.Collections;
using Boo.Lang;
using ControlFreak2;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VehicleMovement : MonoBehaviour
{
	public float speed;						//The current forward speed of the ship

	[Header("Drive Settings")]
	public float driveForce = 17f;			//The force that the engine generates
	public float slowingVelFactor = .99f;   //The percentage of velocity the ship maintains when not thrusting (e.g., a value of .99 means the ship loses 1% velocity when not thrusting)
	public float brakingVelFactor = .95f;   //The percentage of velocty the ship maintains when braking
	public float angleOfRoll = 30f;			//The angle that the ship "banks" into a turn

	[Header("Hover Settings")]
	public float hoverHeight = 1.5f;        //The height the ship maintains when hovering
	public float maxGroundDist = 5f;        //The distance the ship can be above the ground before it is "falling"
	public float hoverForce = 300f;			//The force of the ship's hovering
	public LayerMask whatIsGround;			//A layer mask to determine what layer the ground is on
	public PIDController hoverPID;			//A PID controller to smooth the ship's hovering

	[Header("Physics Settings")]
	public Transform shipBody;				//A reference to the ship's body, this is for cosmetics
	public float terminalVelocity = 100f;   //The max speed the ship can go
	public float hoverGravity = 20f;        //The gravity applied to the ship while it is on the ground
	public float fallGravity = 80f;			//The gravity applied to the ship while it is falling

	[Header("Aman Settings")]
	public float forwardSpeed;

	public TextMeshProUGUI timerText;

	Rigidbody rigidBody;					//A reference to the ship's rigidbody
	PlayerInput input;						//A reference to the player's input
	float drag;								//The air resistance the ship recieves in the forward direction
	bool isOnGround;						//A flag determining if the ship is currently on the ground


	#region  aman used Variables


	public int health;
	public int HealthStatic;

	public GameObject endGameParticle,lowHealthParticle;
	private GameObject particleLowHealth;
	public GameObject missile;
	public Transform firePoint,firePoint1;
	public static VehicleMovement Instance = null;

	private double accValue;
	public GameObject shield;
	public float timeDelayForShoot;
	private float timeStamp;
	private Vector3 accel;
	private float filter = 5.0f;
	private float handlingMultiplier = 3.0f;
	public float handling = 100.0f;
	public float minRoll = -50.0f;
	public float maxRoll = 50.0f;
	public float roll = 10.0f;
	public bool limitsImposed = false;
	public float speed1 = 40.0f;
	public Image healthImage;
	public float CommonSpeed;
	public Transform lasNode;
	public float YforceDamp = 0.1f;
	private Vector3 colPointToMe;
	private float colStrength;
	public float maxCollisionStrength = 50.0f;

	public MeshFilter[] optionalMeshList;

	[SerializeField]
	private MeshFilter[] meshfilters;

	private float sqrDemRange;

	public float maxMoveDelta = 1.0f;

	public float impactDirManipulator = 0.0f;

	public int[] UpgradeSpeedValues;

	#endregion

	#region  MOST USAGEVariables

	Vector3 groundNormal;
	private Ray ray;
	RaycastHit hitInfo;
	private float height;
	private float forcePercent;
	private Vector3 force;
	private Vector3 gravity;
	private Vector3 projection;
	private Quaternion rotation;
	private float angle;
	private Quaternion bodyRotation;
	private float rotationTorque;
	private float sidewaysSpeed;
	private Vector3 sideFriction;
	private float propulsion;
	private Vector3 upwardForceFromCollision;
	private Vector3 acc;
	private bool damagePlane = true;


	#endregion


	private void Awake()
	{
		if (optionalMeshList.Length > 0)
			meshfilters = optionalMeshList;
		else
			meshfilters = GetComponentsInChildren<MeshFilter>();
	}

	void Start()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		//health = 200;
		//Get references to the Rigidbody and PlayerInput components
		rigidBody = GetComponent<Rigidbody>();
		input = GetComponent<PlayerInput>();
		//Calculate the ship's drag value
		drag = driveForce / terminalVelocity;
		StartCoroutine("waitforStartGame");
		HealthStatic = health;
	}

	IEnumerator waitforStartGame()
	{
		yield return new WaitUntil(()=> timerText.text.Equals("Go!"));
		yield return new WaitForSeconds(0.6f);

		int speedCount  = PlayerPrefs.GetInt(this.gameObject.name + "-speedCount");

		if (speedCount == 1)
		{
			CommonSpeed = CommonSpeed + UpgradeSpeedValues[0];
		}
		else if (speedCount == 2)
		{
			CommonSpeed = CommonSpeed + UpgradeSpeedValues[1];
		}
		else if (speedCount == 3)
		{
			CommonSpeed = CommonSpeed + UpgradeSpeedValues[2];
		}
		else
		{
			forwardSpeed = CommonSpeed;
		}


		forwardSpeed = CommonSpeed;
		//after game start planet detail will be off
		GameManager.instance.textureChange.gameObject.SetActive(false);
		GameManager.instance.textureChange.gameObject.transform.parent.gameObject.SetActive(false);
		//after game start control freak will be on
		GameManager.instance.controlFreak.enabled = true;
		//lap UI WILL BE ON
		GameManager.instance.lapUI.enabled = true;
		//mini map will be on
		GameManager.instance.minimap.SetActive(true);
		GameManager.instance.beforeGameStart.SetActive(true);
	}

	private void Update()
	{
		if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.Space) || ControlFreak2.CF2Input.GetButtonDown("Attack"))
		{
			if (Time.time >= timeStamp)
			{
				SoundManager.Instance.SPSound(5);
				Instantiate(missile, firePoint.position, firePoint.rotation);
				Instantiate(missile, firePoint1.position, firePoint1.rotation);
				timeStamp = Time.time + timeDelayForShoot;
			}
		}
	}

	public void Shield()
	{
	//	Debug.Log("Shield");
		SoundManager.Instance.SPSound(9);
		shield.GetComponent<Renderer>().enabled = true;
		shield.GetComponent<ShieldRotate>().visible = true;
		StartCoroutine("ShutOffShield");
	}

	public void Nitro()
	{
	//	Debug.Log("Nitro");
		SoundManager.Instance.SPSound(6);
		forwardSpeed += CommonSpeed + 35f;
		StartCoroutine(DecreaseSpeed());
	}

	IEnumerator ShutOffShield()
	{
		yield return new WaitForSeconds(1.5f);
		SoundManager.Instance.SPSound(7);
		yield return new WaitForSeconds(10f);
		shield.GetComponent<Renderer>().enabled = false;
		shield.GetComponent<ShieldRotate>().visible = false;
	}

	void FixedUpdate()
	{
		//Calculate the current speed by using the dot product. This tells us
		//how much of the ship's velocity is in the "forward" direction
		speed = Vector3.Dot(rigidBody.velocity, transform.forward);
		//Calculate the forces to be applied to the ship
		CalculatHover();
		CalculatePropulsion();
	}

	void CalculatHover()
	{
		//This variable will hold the "normal" of the ground. Think of it as a line
		//the points "up" from the surface of the ground

		//Calculate a ray that points straight down from the ship
		 ray = new Ray(transform.position, -transform.up);

		//Declare a variable that will hold the result of a raycast


		//Determine if the ship is on the ground by Raycasting down and seeing if it hits
		//any collider on the whatIsGround layer
		isOnGround = Physics.Raycast(ray, out hitInfo, maxGroundDist, whatIsGround);

		//If the ship is on the ground...
		if (isOnGround)
		{
			//Debug.Log("Horizontal : " + input.rudder);
			//Debug.Log("Vertical : " + input.thruster);
			//Debug.Log("Accelator : " + Input.acceleration.x);



			if (GameManager.instance.controls == GameManager.MyControl.touch)
			{
				//...determine how high off the ground it is...
				height = hitInfo.distance;
				//...save the normal of the ground...
				groundNormal = hitInfo.normal.normalized;
				//...use the PID controller to determine the amount of hover force needed...
				forcePercent = hoverPID.Seek(hoverHeight, height);

				//...calulcate the total amount of hover force based on normal (or "up") of the ground...
				force = groundNormal * hoverForce * forcePercent;
				//...calculate the force and direction of gravity to adhere the ship to the
				//track (which is not always straight down in the world)...
				gravity = -groundNormal * hoverGravity * height;
				//move ship to forward using rigidbody

				rigidBody.AddForce(new Vector3(transform.forward.x, 0 , transform.forward.z) * forwardSpeed);
				//...and finally apply the hover and gravity forces
				rigidBody.AddForce(force, ForceMode.Acceleration);
				rigidBody.AddForce(gravity, ForceMode.Acceleration);
			}

			else if(GameManager.instance.controls == GameManager.MyControl.acce)
			{
				if (Input.acceleration.x > 0 && Input.acceleration.x <= 1)
				{
					accValue += 0.02 * 1.1f;
				}
				else if (Input.acceleration.x < 0 && Input.acceleration.x >= -1)
				{
					accValue -= 0.02 * 1.1f;
				}

				//...determine how high off the ground it is...
				height = hitInfo.distance;
				//...save the normal of the ground...
				groundNormal = hitInfo.normal.normalized;
				//...use the PID controller to determine the amount of hover force needed...
				forcePercent = hoverPID.Seek(hoverHeight, height);
				//...calulcate the total amount of hover force based on normal (or "up") of the ground...
				force = groundNormal * hoverForce * forcePercent;
				gravity = -groundNormal * hoverGravity * height;
				//move ship to forward using rigidbody
				rigidBody.AddForce(new Vector3(transform.forward.x, 0 , 0) * forwardSpeed);
				//	transform.Translate(Input.acceleration.x,0,0);
				//Debug.Log("Accelerator :" + "X:" + Input.acceleration.x + " Y:" + Input.acceleration.y + " Z:" + Input.acceleration.z);
				rigidBody.AddForce(force, ForceMode.Acceleration);
				rigidBody.AddForce(gravity, ForceMode.Acceleration);

				// new code
				Vector3 dir = Vector3.zero;

				//Gets the Accelerometer values from Input.cceleration
				accel = Vector3.Lerp(accel, Input.acceleration, filter * Time.deltaTime);
				transform.eulerAngles = new Vector3(transform.eulerAngles.x,
					transform.eulerAngles.y,Mathf.Clamp(-(accel.x * roll) * roll, minRoll, maxRoll));
				shipBody.eulerAngles = new Vector3(shipBody.eulerAngles.x,
					shipBody.eulerAngles.y,Mathf.Clamp(-(accel.x * roll) * roll, minRoll, maxRoll));

				//Accel.x is used to tilt it from left to right
				dir.x = (accel.x * handling) * handlingMultiplier;
				dir.x *= handlingMultiplier; //Exagerate x in left and right directions
				//Lets allow acceleration and stop. This allow you to tilt the phone forward or
				//backward to slower acceleration.
				var accelerometerForwardBackwardMultiplier = 2.0f;
				var forwardSpeed1 = (accel.z * -1) * accelerometerForwardBackwardMultiplier;
				dir.z = speed * (forwardSpeed1);


				//Accelerate based on limits if allowPredefinedLimitsImposed is enabled
				if(limitsImposed){
					if(dir.z >= 40.0f && dir.z <= 65.0f){
						dir.z *= 2;
					} else if(dir.z >= 66.0f && dir.z <= 80.0f){
						dir.z *= 5;
					} else if(dir.z >= 81.0f) {
						dir.z *= 6;
					}
				}
				// Make it move 10 meters per second instead of 10 meters per frame...
				dir *= Time.deltaTime;
				//Use Rigid Body to make it more realistic
				transform.GetComponent<Rigidbody>().AddRelativeForce(dir * speed1);
			}
		}
		//...Otherwise...
		else
		{
			//...use Up to represent the "ground normal". This will cause our ship to
			//self-right itself in a case where it flips over
			groundNormal = Vector3.up;
			//Calculate and apply the stronger falling gravity straight down on the ship
			 gravity = -groundNormal * fallGravity;
			rigidBody.AddForce(gravity, ForceMode.Acceleration);
		}

		//Calculate the amount of pitch and roll the ship needs to match its orientation
		//with that of the ground. This is done by creating a projection and then calculating
		//the rotation needed to face that projection
		 projection = Vector3.ProjectOnPlane(transform.forward, groundNormal);
		 rotation = Quaternion.LookRotation(projection, groundNormal);

		//Move the ship over time to match the desired rotation to match the ground. This is
		//done smoothly (using Lerp) to make it feel more realistic
			rigidBody.MoveRotation(Quaternion.Lerp(rigidBody.rotation, rotation, Time.deltaTime * 10f));

		//Calculate the angle we want the ship's body to bank into a turn based on the current rudder.
		//It is worth noting that these next few steps are completetly optional and are cosmetic.
		//It just feels so darn cool

		//angle = angleOfRoll * -input.rudder;

		if (GameManager.instance.controls == GameManager.MyControl.touch)
		{
			angle = angleOfRoll * -input.rudder;
			bodyRotation = transform.rotation * Quaternion.Euler(0f, 0f, angle);
			shipBody.rotation = Quaternion.Lerp(shipBody.rotation, bodyRotation, Time.deltaTime * 10f);
		}
		else if (GameManager.instance.controls == GameManager.MyControl.acce)
		{
			angle = angleOfRoll * -Input.acceleration.x;
		//	angle = angleOfRoll * -(float)accValue;
			bodyRotation = transform.rotation * Quaternion.Euler(0f, 0f, angle);
			shipBody.rotation = Quaternion.Lerp(shipBody.rotation, bodyRotation, Time.deltaTime * 10f);
		}

		//Calculate the rotation needed for this new angle

		//Finally, apply this angle to the ship's body

	}

	private void SetVelocityZero()
	{
		rigidBody.velocity = Vector3.zero;
	}

	private void MoveRight()
	{
		rigidBody.velocity = new Vector3(15f,0,0);
	}

	private void MoveLeft()
	{
		rigidBody.velocity = new Vector3(-15f,0,0);
	}


	void CalculatePropulsion()
	{
		//Calculate the yaw torque based on the rudder and current angular velocity

		//	rotationTorque = input.rudder - rigidBody.angularVelocity.y;

		if (GameManager.instance.controls == GameManager.MyControl.touch)
		{
			rotationTorque = input.rudder - rigidBody.angularVelocity.y;
		}
		else if (GameManager.instance.controls == GameManager.MyControl.acce)
		{
		rotationTorque = Input.acceleration.x - rigidBody.angularVelocity.y;

		}

		//rotationTorque = (float)accValue - rigidBody.angularVelocity.y;

		//Apply the torque to the ship's Y axis
		rigidBody.AddRelativeTorque(0f, rotationTorque, 0f, ForceMode.VelocityChange);

		//Calculate the current sideways speed by using the dot product. This tells us
		//how much of the ship's velocity is in the "right" or "left" direction
		 sidewaysSpeed = Vector3.Dot(rigidBody.velocity, transform.right);

		//Calculate the desired amount of friction to apply to the side of the vehicle. This
		//is what keeps the ship from drifting into the walls during turns. If you want to add
		//drifting to the game, divide Time.fixedDeltaTime by some amount
		 sideFriction = -transform.right * (sidewaysSpeed / Time.fixedDeltaTime);

		//Finally, apply the sideways friction
		rigidBody.AddForce(sideFriction, ForceMode.Acceleration);

		//If not propelling the ship, slow the ships velocity

			if (input.thruster <= 0f)
			rigidBody.velocity *= slowingVelFactor;

		//Braking or driving requires being on the ground, so if the ship
		//isn't on the ground, exit this method
		if (!isOnGround)
			return;

		//If the ship is braking, apply the braking velocty reduction
		if (input.isBraking)
			rigidBody.velocity *= brakingVelFactor;

		//Calculate and apply the amount of propulsion force by multiplying the drive force
		//by the amount of applied thruster and subtracting the drag amount


			propulsion = driveForce * input.thruster - drag * Mathf.Clamp(speed, 0f, terminalVelocity);
			rigidBody.AddForce(transform.forward * propulsion, ForceMode.Acceleration);
	}



	public float GetSpeedPercentage()
	{
		//Returns the total percentage of speed the ship is traveling
		return rigidBody.velocity.magnitude / terminalVelocity;
	}

	private void OnTriggerEnter(Collider other)
	{
//		Debug.Log(other.gameObject.name);
		if (other.gameObject.CompareTag("PlayerMissile"))
		{
			return;
		}

		if (other.gameObject.CompareTag("obsInc"))
		{
			forwardSpeed = CommonSpeed + 35f;
			SoundManager.Instance.SPSound(8);
			VehicleAVFX.Instance.exhaustLight.intensity = 4f;
			VehicleAVFX.Instance.exhaustLight.color = new Color(.3f, .6f, .8f);
			StartCoroutine("DecreaseSpeed");
		}

		else if (other.gameObject.CompareTag("obsDes"))
		{
			forwardSpeed -= 20f;
			SoundManager.Instance.SPSound(8);
			VehicleAVFX.Instance.exhaustLight.intensity = 4f;
			VehicleAVFX.Instance.exhaustLight.color = new Color(.3f, .6f, .8f);
			StartCoroutine("DecreaseSpeed");
		}

		if (other.gameObject.transform.root.gameObject.CompareTag("EnemyMissile"))
		{
			health -= 5;

			//float fillValue = health / HealthStatic;

			float fillvalue = (float) (
				                  (double) health / HealthStatic * 100) / 100;

			healthImage.fillAmount = fillvalue;
		}

		if (other.gameObject.GetComponent<WayPointValue>())
		{
			if (other.gameObject.name == "Waypoint 0")
			{
				return;
			}

			for (int i = 0; i < RaceManager.Instance.path.waypoints.Length; i++)
			{
				if (other.gameObject.name == RaceManager.Instance.path.waypoints[i].name)
				{
					lasNode = RaceManager.Instance.path.waypoints[i - 1];
				}
			}
		}

		GameManager.instance.CurrentHealth();

	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag("PlayerMissile"))
		{
			return;
		}


		if (other.gameObject.layer == 10)
		{
			health -= 5;
			float fillvalue = (float) (
				(double) health / HealthStatic * 100) / 100;

			//float fillValue = health / HealthStatic;
			healthImage.fillAmount = fillvalue;
		}

		if (health >= 20 && health <= 50 && damagePlane == true)
		{
			damagePlane = false;
			particleLowHealth = Instantiate(lowHealthParticle, transform.position, Quaternion.identity);
			particleLowHealth.transform.parent = gameObject.transform;
		}

		if (health == 50)
		{
			SoundManager.Instance.SPSound(10);
		}

		if (health <= 0)
		{
			DestroyMyPlayer();
		}



		if (shield.GetComponent<ShieldRotate>().visible)
		{
			return;
		}
		//If the ship has collided with an object on the Wall layer...
		if (other.gameObject.layer == LayerMask.NameToLayer("Wall"))
		{
			if (shield.GetComponent<ShieldRotate>().visible)
			{
				return;
			}
			//...calculate how much upward impulse is generated and then push the vehicle down by that amount
			//to keep it stuck on the track (instead up popping up over the wall)
			/*upwardForceFromCollision = Vector3.Dot(collision.impulse, transform.right) * transform.right;
		 rigidBody.AddForce(-upwardForceFromCollision, ForceMode.Impulse);*/

		//	Handheld.Vibrate();

			Vector3 colRelVel = other.relativeVelocity;
			colRelVel.y *= YforceDamp;
			if (other.contacts.Length > 0)
			{
//				Debug.Log("Touch Wall");
				colPointToMe = transform.position - other.contacts[0].point;
				colStrength = colRelVel.magnitude * Vector3.Dot
					              (other.contacts[0].normal, colPointToMe.normalized);
				if (colPointToMe.magnitude > 1.0f)
				{
					//play crash sound here
					SoundManager.Instance.SPSound(2);
					OnMeshForce(other.contacts[0].point, Mathf.Clamp01(colStrength / maxCollisionStrength));
				}

			}

		}

		GameManager.instance.CurrentHealth();
	}

	private void OnMeshForce(Vector3 originPos, float force)
	{
		force = Mathf.Clamp01(force);
		for (int j = 0; j < meshfilters.Length; ++j)
		{
			Vector3[] verts = meshfilters[j].mesh.vertices;
			for (int i = 0; i < verts.Length; ++i)
			{
				Vector3 scaleVert = Vector3.Scale(verts[i], transform.localScale);
				Vector3 vertWorldPos = meshfilters[j].transform.position +
				                       (meshfilters[j].transform.rotation * scaleVert);
				Vector3 originToMeDir = vertWorldPos - originPos;
				Vector3 flatVertToCenterDir = transform.position - vertWorldPos;
				flatVertToCenterDir.y = 0.0f;

				if (originToMeDir.sqrMagnitude < sqrDemRange)
				{
					float dist = Mathf.Clamp01(originToMeDir.sqrMagnitude / sqrDemRange);
					float moveDelta = force * (1.0f - dist) * maxMoveDelta;
					Vector3 moveDir =
						Vector3.Slerp(originToMeDir, flatVertToCenterDir, impactDirManipulator).normalized * moveDelta;
					verts[i] += Quaternion.Inverse(transform.rotation) * moveDir;
				}

			}

			meshfilters[j].mesh.vertices = verts;
			meshfilters[j].mesh.RecalculateBounds();
		}

	}


	public void DestroyMyPlayer()
	{
		//crash set
		GameManager.instance.crash_out_title.GetComponent<RawImage>().transform.parent.gameObject.SetActive(true);
		GameManager.instance.crash_out_title.GetComponent<RawImage>().texture =
			GameManager.instance.OutOfRank_Crash[0];
	//	GameManager.instance.crash_out_title.gameObject.transform.parent.transform.GetChild(4).gameObject.SetActive(true);
	//	GameManager.instance.crash_out_title.gameObject.transform.parent.transform.GetChild(3).gameObject.SetActive(false);

		GameManager.instance.crash_out_title.gameObject.transform.parent.transform.GetChild(3).gameObject.SetActive(false);

		PlayerPrefs.SetInt("DestroyMyPlayer", 10);

		GameManager.instance.controlFreak.enabled = false;
		Destroy(this.particleLowHealth);
		Instantiate(endGameParticle, transform.position, Quaternion.identity);
		Destroy(this.gameObject);
	}

	IEnumerator DecreaseSpeed()
	{
		yield return new WaitForSeconds(8f);
		forwardSpeed = CommonSpeed;
	}
}
