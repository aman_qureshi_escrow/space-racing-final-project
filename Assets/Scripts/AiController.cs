﻿using System.Collections;
using System.Collections.Generic;
using SWS;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class AiController : MonoBehaviour
{
	private Rigidbody rigidBody;
	public ParticleSystem engineThrusterR, engineThrusterL;
	public TextMeshProUGUI timerText;
	private bool isActiveGame;
	[SerializeField]
	float[] lapTimes;
	[SerializeField]
	private string lapTimeString;

	[SerializeField]
	public string FinalTimeString;

	#region  MOST USAGEVariables

	Vector3 groundNormal;
	private Ray ray;
	RaycastHit hitInfo;
	private float height;
	private float forcePercent;
	private Vector3 force;
	private Vector3 gravity;


	private float angle;
	private Quaternion bodyRotation;
	private float rotationTorque;
	private float sidewaysSpeed;


	public PIDController hoverPID;
	public float hoverGravity = 20f;        //The gravity applied to the ship while it is on the ground
	public float hoverHeight = 1.5f;
	public float hoverForce = 300f;
	public float maxGroundDist = 5f;
	public LayerMask whatIsGround;
	bool isOnGround;

	public bool GameOver = false;
	public float finalTimeFloat;

	// gravity constant
	#endregion



	// Use this for initialization
	void Start ()
	{
		rigidBody = GetComponent<Rigidbody>();
		StartCoroutine("waitforStartGame");
		var main = engineThrusterL.main;
		main.loop = true;
		var main1 = engineThrusterR.main;
		main1.loop = true;
		/*engineThrusterR.Play();
		engineThrusterL.Play();*/
		isActiveGame = false;
	}

	private void FixedUpdate()
	{

		//Calculate a ray that points straight down from the ship
		ray = new Ray(transform.position, -transform.up);

		//Declare a variable that will hold the result of a raycast

		//Determine if the ship is on the ground by Raycasting down and seeing if it hits
		//any collider on the whatIsGround layer
		isOnGround = Physics.Raycast(ray, out hitInfo, maxGroundDist, whatIsGround);
		//...determine how high off the ground it is...
		height = hitInfo.distance;
		//...save the normal of the ground...
		groundNormal = hitInfo.normal.normalized;
		//...use the PID controller to determine the amount of hover force needed...
		forcePercent = hoverPID.Seek(hoverHeight, height);

		//...calulcate the total amount of hover force based on normal (or "up") of the ground...
		force = groundNormal * hoverForce * forcePercent;
		//...calculate the force and direction of gravity to adhere the ship to the
		//track (which is not always straight down in the world)...
		gravity = -groundNormal * hoverGravity * height;
		//move ship to forward using rigidbody

		rigidBody.AddForce(force, ForceMode.Acceleration);
		rigidBody.AddForce(gravity, ForceMode.Acceleration);

	}

	IEnumerator waitforStartGame()
	{
		yield return new WaitUntil(()=> timerText.text.Equals("Go!"));
		yield return new WaitForSeconds(0.6f);
		GetComponent<splineMove>().StartMove();
		lapTimes = new float[GameManager.instance.numberOfLaps];
		isActiveGame = true;
		float commonspeed = GameManager.instance.vehicleMovement.CommonSpeed;
		float AIDecidedSpeed = Random.Range(commonspeed, commonspeed + 40f);
		GetComponent<splineMove>().speed = AIDecidedSpeed;
		//GetComponent<splineMove>().speed = 25f;
	}

	private void Update()
	{
//		Debug.Log("Current :" + GetComponent<Ship>().currentLap);
	//	Debug.Log("Number Of lap  :" + GameManager.instance.numberOfLaps);

		if (GetComponent<Ship>().currentLap >= GameManager.instance.numberOfLaps)
		{
			GetComponent<splineMove>().enabled = false;
			GameOver = true;
			return;
		}

		if (isActiveGame)
		{
			//Debug.Log(lapTimes[GetComponent<Ship>().currentLap]);
			lapTimes[GetComponent<Ship>().currentLap] += Time.deltaTime;
			UpdateUI_Lap_Time();
		}
	}

	private void UpdateUI_Lap_Time()
	{
		SetLapTime(GetComponent<Ship>().currentLap, lapTimes[GetComponent<Ship>().currentLap]);
	}

	private void SetLapTime(int currentLap, float lapTime)
	{
		lapTimeString = ConvertTimeToString(lapTime);
	}

	private string ConvertTimeToString(float lapTime)
	{
		//takes time and convert it into the number of minutes and seconds
		int minutes = (int) (lapTime / 60);
		float seconds = lapTime % 60f;

		//create the string in the appropriae format for the time
		string output = minutes.ToString("00") + ":" + seconds.ToString("00.000");
		return output;
	}

	public void PlayerCompletedLap()
	{
		if (GetComponent<Ship>().currentLap >= GameManager.instance.numberOfLaps)
		{
			isActiveGame = false;
			UpdateUI_FinalTime();
		}
	}

	private void UpdateUI_FinalTime()
	{
		float total = 0f;
		for (int i = 0; i < lapTimes.Length; i++)
		{
			total += lapTimes[i];
		}

		SetFinalTime(total);
	}

	private void SetFinalTime(float total)
	{
		finalTimeFloat = total;
//		Debug.Log(this.gameObject.name + total);
		FinalTimeString = ConvertTimeToString(total);
	}
}
