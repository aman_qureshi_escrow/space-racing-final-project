﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldRotate : MonoBehaviour
{

	private float rotationSpeed = 5f;

	public bool visible;
	// Use this for initialization
	void Start ()
	{
		visible = false;
		GetComponent<MeshRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(visible)
		transform.Rotate(0,rotationSpeed,0);
	}
}
