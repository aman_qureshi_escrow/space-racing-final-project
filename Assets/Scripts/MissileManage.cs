﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileManage : MonoBehaviour
{

	private Rigidbody rb;
	public float speed;

	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		//StartCoroutine("DestroyThis");
		this.gameObject.GetComponent<Rigidbody>().
		AddForce(transform.forward * speed, ForceMode.VelocityChange);
	}

	private void OnCollisionEnter(Collision other)
	{
		Destroy(this.gameObject);
	}
}
