﻿//This script handles letting the game manager know when the player completes a lap. It
//works together with the LapChecker script to ensure that the player can't cheat

using System.Linq;
using Boo.Lang;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinishLine : MonoBehaviour
{
	[HideInInspector]	public bool isReady;	//Is the player ready to complete a lap?
	public bool debugMode;						//Debug variable that enables quick testing of laps
	[SerializeField]
	private int playerCounter;
	[SerializeField]
	private GameObject lastObj;
	[SerializeField]
	private int DestroyedShip;

	[SerializeField]
	private int remainingPlayer;

	private void Start()
	{
		//5
		remainingPlayer = 5;
	}


	//Called when the player drives through the finish line
	void OnTriggerEnter(Collider other)
	{
		//If the player has passed through the LapChecker (isRead) OR if Debug Mode is enabled (debugMode)
		//AND the object passing through this trigger is tagged as "PlayerSensor"...
		if ((isReady || debugMode) && other.gameObject.CompareTag("PlayerSensor"))
		{
			//...let the Game Manager know that the player completed a lap...
			GameManager.instance.PlayerCompletedLap();
			//...and deactivate the finish line until the player completes another lap
			isReady = false;
		}



		if (SceneManager.GetActiveScene().name == "Venus1" || SceneManager.GetActiveScene().name == "Mars1")
		{
			if (other.gameObject.transform.parent.gameObject.GetComponent<Ship>().triggerValue.Equals(1))
			{
				Debug.Log(other.gameObject.transform.parent.gameObject.name);
				other.gameObject.transform.parent.gameObject.GetComponent<Ship>().triggerValue = 0;

				other.gameObject.transform.parent.gameObject.GetComponent<Ship>().TriggerValueChange();

				playerCounter++;

				//3
				if (DestroyedShip >= 1)
				{
					remainingPlayer--;
				}

				if (playerCounter >= remainingPlayer)
				{

					playerCounter = 0;
					remainingPlayer--;
					lastObj = other.gameObject.transform.parent.gameObject;

					//des 3
					if (DestroyedShip >= 1)
					{

						if (lastObj.CompareTag("Player"))
						{
							Debug.Log(" des player");
							lastObj.SetActive(false);
							GameManager.instance.Venus1Eliminate();
							Time.timeScale = 0f;

							//	lastObj.GetComponent<VehicleMovement>().DestroyMyPlayer();
						}
						else
						{
							Debug.Log("des enemy");
							GameManager.instance.crash_out_title.GetComponent<RawImage>().transform.parent.gameObject.SetActive(true);
							GameManager.instance.crash_out_title.GetComponent<RawImage>().texture =
								GameManager.instance.OutOfRank_Crash[0];
							GameManager.instance.controlFreak.enabled = false;
						}

					}
					else
					{
						if (lastObj.CompareTag("Player"))
						{
							Debug.Log("Last player");
							DestroyedShip++;
							//crash set
							/*GameManager.instance.crash_out_title.GetComponent<RawImage>().transform.parent.gameObject.SetActive(true);
							GameManager.instance.crash_out_title.GetComponent<RawImage>().texture =
								GameManager.instance.OutOfRank_Crash[0];*/

							GameManager.instance.SetScoreBoard();
							GameManager.instance.controlFreak.enabled = false;
							lastObj.SetActive(false);
							//	lastObj.GetComponent<VehicleMovement>().DestroyMyPlayer();
						}
						else
						{
							Debug.Log("last enemy");
							DestroyedShip++;
							lastObj.SetActive(false);
							//	Destroy(lastObj);
						}
					}

				}
			}

		}

	}
}
