﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObj : MonoBehaviour
{
	private bool Rotate = false;
	private float Value = 1;

//	public GameObject paricle;

	[SerializeField]
	public enum Collectable
	{
		Crystal,Diamond,shield,nitro
	}


	private Collectable coll;

	private void Start()
	{

		//PlayerPrefs.DeleteAll();

		if (this.gameObject.CompareTag("Crystal"))
		{
			coll = Collectable.Crystal;
		}
		else if(this.gameObject.CompareTag("Diamond"))
		{
			coll = Collectable.Diamond;
		}
		else if(this.gameObject.CompareTag("shield"))
		{
			coll = Collectable.shield;
		}
		else if(this.gameObject.CompareTag("nos"))
		{
			coll = Collectable.nitro;
		}

	//	StartCoroutine(DelayRotate());
	}

	private void Update()
	{
		if (Rotate)
		{
			transform.Rotate(0,+Value,0);
		}
		else
		{
			transform.Rotate(0,-Value,0);
		}
	//	Debug.Log(PlayerPrefs.GetInt("PlayAreaCrystal"));
//		Debug.Log(PlayerPrefs.GetInt("PlayAreaDiamond"));

	}

	/*IEnumerator DelayRotate()
	{
		float RandomValue = Random.Range(2, 5);
		Rotate = true;
		yield return new WaitForSeconds(RandomValue);
		Rotate = false;
		yield return new WaitForSeconds(RandomValue);
		StartCoroutine(DelayRotate());
	}*/

	private void OnTriggerEnter(Collider other)
	{
//		Debug.Log(other.gameObject.transform.parent.gameObject.name);
	//	Debug.Log(other.gameObject.transform.parent.gameObject.tag);

		if (other.gameObject.transform.parent.CompareTag("Player") && other.gameObject.transform.parent.GetComponent<VehicleMovement>() != null)
		{

			if (coll == Collectable.Diamond)
			{
				PlayerPrefs.SetInt("PlayAreaDiamond", PlayerPrefs.GetInt("PlayAreaDiamond") + 1);
				GameManager.instance.vehicleMovement.GetComponent<VehicleAVFX>().OnGem(0);
			//	Instantiate(paricle, transform.position, transform.rotation);
			}
			else if (coll == Collectable.Crystal)
			{
				PlayerPrefs.SetInt("PlayAreaCrystal", PlayerPrefs.GetInt("PlayAreaCrystal") + 100);
				GameManager.instance.vehicleMovement.GetComponent<VehicleAVFX>().OnGem(1);
			//	Instantiate(paricle, transform.position, transform.rotation);
			}

			else if (coll == Collectable.shield)
			{
				PlayerPrefs.SetInt("useShield", PlayerPrefs.GetInt("useNitro") + 1);
				GameManager.instance.RemainingNitroShield();
				//GameManager.instance.vehicleMovement.GetComponent<VehicleAVFX>().OnGem(1);
				//	Instantiate(paricle, transform.position, transform.rotation);
			}

			else if (coll == Collectable.nitro)
			{
				PlayerPrefs.SetInt("useNitro", PlayerPrefs.GetInt("useNitro") + 1);
				GameManager.instance.RemainingNitroShield();
				//GameManager.instance.vehicleMovement.GetComponent<VehicleAVFX>().OnGem(1);
				//	Instantiate(paricle, transform.position, transform.rotation);
			}

			AiManager.Instance.UpdateValues();
			Destroy(this.gameObject);
		}
	}
}
