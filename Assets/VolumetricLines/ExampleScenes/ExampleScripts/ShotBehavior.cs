﻿using UnityEngine;
using System.Collections;

public class ShotBehavior : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{

		StartCoroutine("DestroyMyObject");
	}


	IEnumerator DestroyMyObject()
	{
		yield return new WaitForSeconds(2.5f);
		Destroy(this.gameObject);
	}


	// Update is called once per frame
	void Update () {

		if (this.gameObject != null)
		{
			transform.position += transform.forward * Time.deltaTime * 1000f;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
//		Debug.Log( "OnTriggerEnter" + other.gameObject.transform.parent.gameObject.name);
	}

	private void OnCollisionEnter(Collision other)
	{
	//	Debug.Log( "OnCollisionEnter" + other.gameObject.transform.parent.gameObject.name);
	}
}
