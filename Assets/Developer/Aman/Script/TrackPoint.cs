﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using SWS;
using UnityEngine;
using UnityEngine.Networking;

public class TrackPoint : MonoBehaviour
{
    [Header("Debug")] public bool ShowLine = false;

    private GameObject trackPoint;
    private int activeWayPointIndex = 0;

    private Vector3 lineStart;
    private Vector3 lineEnd;

    private float distanceToEndPoint;
    private float distanceToStartPoint;

    private float tempDistanceToEndPoint = -1f;
    private float tempActiveWayPoint;

    public PathManager path;

    // Use this for initialization
    void Start()
    {
        trackPoint = new GameObject();
        trackPoint.name = "TrackPoint_" + name;
    }

    // Update is called once per frame
    void Update()
    {
        SetPosition();
        SetActiveWayPoint();
        /*RankManager.Instance.SetRank(new Player()
        {
            activeWayPointIndex = this.activeWayPointIndex,
            distanceToWayPoint = distanceToEndPoint,
            name = this.name
        });
        if (ShowLine)
        {
            Debug.DrawLine(transform.position, trackPoint.transform.position, Color.magenta);
        }*/
    }

    private void SetPosition()
    {
        lineStart = path.waypoints[activeWayPointIndex].position;
//        Debug.Log(lineStart);
        lineEnd = lineStart;
        if (activeWayPointIndex < path.waypoints.Length - 1)
        {
            lineEnd = path.waypoints[activeWayPointIndex + 1].position;
        }

        Vector3 normal = (lineStart - lineEnd).normalized;
        Vector3 pos = lineStart + Vector3.Project(transform.position - lineStart, normal);
        pos.x = Mathf.Clamp(pos.x, Mathf.Min(lineStart.x, lineEnd.x), Mathf.Max(lineStart.x, lineEnd.x));
        pos.z = Mathf.Clamp(pos.x, Mathf.Min(lineStart.z, lineEnd.z), Mathf.Max(lineStart.z, lineEnd.z));

        trackPoint.transform.position = pos;
    }

    private void SetActiveWayPoint()
    {
        distanceToEndPoint = Vector3.Distance(lineEnd, trackPoint.transform.position);
        distanceToStartPoint = Vector3.Distance(lineStart, trackPoint.transform.position);

        if (0.1 >= distanceToEndPoint)
        {
            if (activeWayPointIndex < path.waypoints.Length - 1)
            {
                tempDistanceToEndPoint = -1f;
                activeWayPointIndex++;
            }
        }
        else if (0.1 >= distanceToStartPoint)
        {
            if (activeWayPointIndex > 0)
            {
                tempDistanceToEndPoint = -1f;
                activeWayPointIndex--;
            }
        }
    }
}