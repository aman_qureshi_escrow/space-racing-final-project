﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {

	public string name { get; set; }
	public int activeWayPointIndex { get; set; }
	public float distanceToWayPoint { get; set; }

	public override string ToString()
	{
		return activeWayPointIndex.ToString();
	}
	
}
