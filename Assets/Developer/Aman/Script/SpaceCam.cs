﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceCam : MonoBehaviour
{

	public static SpaceCam Instance;


	#region camera Variables

	public float distance = 5.0f;
	public float smooth = 0.3f;
	public float height = 1.0f;
	public float angle = 20;
	public LayerMask lineOfSightMask = 0;


	[SerializeField]
	public Transform target;
	private float farDistance;
	private Vector3 farCameraPosition;
	private bool farCameraView = false;
	private float currentDistance;
	private Vector3 velocity = Vector3.zero;
	private float Xsmooth;
	private float zAngleAmount = 0.0f;
	#endregion


	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}

		farCameraPosition = transform.position;
		target = GameObject.FindWithTag("Player").transform;
	}

	// Use this for initialization
	void Start () {
		farCameraView = true;
	}

	// Update is called once per frame
	private void LateUpdate()
	{
		if (GameManager.instance.controls == GameManager.MyControl.acce)
		{
			farDistance = Vector3.Distance(farCameraPosition, target.position);
			if (farDistance < 100.0f && farCameraView)
			{
				farCameraView = false;
			}

			RenderSettings.flareStrength = 0.3f;
			GetComponent<Camera>().fieldOfView =
				Mathf.Clamp(target.GetComponent<VehicleMovement>().CommonSpeed / 10.0f + 60.0f, 60, 90f);

			currentDistance = distance;
			float yAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, target.eulerAngles.y, ref velocity.y, smooth);
			float xAngle = Mathf.SmoothDampAngle(transform.eulerAngles.x, target.eulerAngles.x + (angle),
				ref velocity.x, smooth);

			//look at target
			transform.eulerAngles = new Vector3(xAngle,yAngle,AccelerationAngle());

			Xsmooth = Mathf.Lerp(Xsmooth, velocity.y, Time.deltaTime * 10.0f);
			var Direction = transform.rotation * -new Vector3(-Xsmooth / 300.0f, 0, 1);
			var targetDistance = AdjustLineOfSight(target.position + new Vector3(0, height, 0), Direction);
			transform.position = target.position + new Vector3(0, height, 0) + Direction * targetDistance;

		}
	}

	private float AdjustLineOfSight(Vector3 vector3, Vector3 direction)
	{
		RaycastHit hit;
		if (Physics.Raycast(target.position, direction, out hit, currentDistance, lineOfSightMask.value))
		{
			return hit.distance;
		}
		else
		return currentDistance;

	}

	private float AccelerationAngle()
	{
		zAngleAmount = Mathf.Clamp(zAngleAmount, -45.0f, 45.0f);
		zAngleAmount = Mathf.Lerp(zAngleAmount, Input.acceleration.x * -70.0f, Time.deltaTime * 2.0f);
		return zAngleAmount;
	}
}
