﻿using UnityEngine;
using System.Collections;

public class GizmoObject : MonoBehaviour
{
    private RaycastHit hit;
    
    public Color GizmoColor = Color.white;
    public float GizmoSize = 1.0f;
    private float range = 100f;


    void OnDrawGizmos()
    {
        Vector3 direction = transform.TransformDirection(Vector3.forward) * 2.0f;

        Gizmos.color = GizmoColor;
        Gizmos.DrawRay(transform.position, direction);

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one * GizmoSize);
        Gizmos.matrix = rotationMatrix;

        Gizmos.DrawCube(Vector3.zero, Vector3.one * GizmoSize);
    }

    private void Update()
    {
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward), Color.green, range);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, range))
        {
            if (hit.collider.gameObject.transform ==
                transform.parent.gameObject.GetComponent<VehicleMovement>().lasNode)
            {
                //Debug.Log("Wrong Turn");
            }
        }
    }
}
