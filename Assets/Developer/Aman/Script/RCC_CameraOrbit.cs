﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2015 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using System.Collections;

[AddComponentMenu ("BoneCracker Games/Realistic Car Controller/Camera/Orbit")]
public class RCC_CameraOrbit : MonoBehaviour
{

	public Transform target;
	public float distance = 10.0f;
	
	public float xSpeed = 250f;
	public float ySpeed = 120f;
	
	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;
	
	private float x = 0f;
	private float y = 0f;

	private Transform myTransform, cameraTransform;
	private float rotation1 = 0f;
	private float rotation1_X = 0f;
//	Vector3 moveDirection = Vector3.zero;

	void  Start ()
	{

		Vector3 angles = transform.eulerAngles;
		//starting angles 0,0,0
		//Debug.Log("Angles : " + angles);
		x = 0;
//		y = angles.x;

		y = ClampAngle (y, yMinLimit, yMaxLimit);
	
	//	Debug.Log("y" + y);
		
		Quaternion rotation = Quaternion.Euler (y, x, 0);
	//	Debug.Log("ROtation : " + rotation);
		Vector3 position = rotation * new Vector3 (4f, 57.6818f, -distance - 62.1f) + target.position;
	//	Debug.Log("Position : " + position);
		transform.rotation = rotation;
		transform.position = position;

	}

	public void Last_Rotation ()
	{
		Vector3 angles = transform.eulerAngles;

		x = angles.y;
		y = angles.x;
	}


	public void PlayerRotation (float horizontal, float vertical)
	{
		
//		myTransform.Rotate (0f, horizontal * 3f, 0f);   // Default value  * 12.....
		rotation1 = vertical * 8f;
		rotation1_X = horizontal * 10f;
//		rotation1 = Mathf.Clamp (rotation1, -60f, 60f);
//		cameraTransform.localEulerAngles = new Vector3 (-rotation1, cameraTransform.localEulerAngles.y, 0f);



		x += rotation1_X;
		y -= rotation1;



		y = ClampAngle (y, yMinLimit, yMaxLimit);

		Quaternion rotation = Quaternion.Euler (y, x, 0);
		Vector3 position = rotation * new Vector3 (4f, 57.6814f, -distance - 62.1f) + target.position;

		transform.rotation = rotation;
		transform.position = position;
	}

	void  LateUpdate ()
	{
		if (target) {

//			x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
//			y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

//			x += rotation1_X;
//			y -= rotation1;
//
//			y = ClampAngle (y, yMinLimit, yMaxLimit);
//			


//			Quaternion rotation = Quaternion.Euler (y, x, 0);
//			Vector3 position = rotation * new Vector3 (0f, 2f, -distance) + target.position;
//			
//			transform.rotation = rotation;
//			transform.position = position;
		}

	}

	static float ClampAngle (float angle, float min, float max)
	{
	
		//Debug.Log("Angle :" + angle + "Min :" + min + "Max:" + max);
		// angle 0 min -40 max  100
		//angle 0
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
	//	Debug.Log("Clmap :" + Mathf.Clamp(angle, min,max));
		return Mathf.Clamp (angle, min, max);

	}
	
}