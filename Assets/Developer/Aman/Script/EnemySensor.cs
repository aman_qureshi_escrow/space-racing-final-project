﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySensor : MonoBehaviour
{

	public GameObject rootObj;
	
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("FinishLiNE"))
		{
//			Debug.Log(other.gameObject.name);
			rootObj.GetComponent<AiController>().PlayerCompletedLap();
		}
	}
}
