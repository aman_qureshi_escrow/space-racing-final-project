﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SharingAnIO : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Sharing()
	{
		#if UNITY_ANDROID
		StartCoroutine(AndroidSharing());
		#endif
	
		#if UNITY_IOS
		IosSharing();
		#endif
	}

	IEnumerator AndroidSharing()
	{
		yield return new WaitForEndOfFrame();
		//create Refrence of Android Java Class Class For Intent
		AndroidJavaClass intentClass = new AndroidJavaClass
			("android.content.Intent");
		//create Refrence of android Java Object class Intent
		AndroidJavaObject intentObj = new AndroidJavaObject
			("android.content.Intent");
		
		//set action for intent
		intentObj.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		intentObj.Call<AndroidJavaObject>("setType", "text/plain");
		intentObj.Call<AndroidJavaObject>("putExtra",intentClass.GetStatic<string>("EXTRA_SUBJECT"), "Hower Racer");
		
		intentObj.Call<AndroidJavaObject>("putExtra",intentClass.GetStatic<string>("EXTRA_TITLE"), "Check Out Our Game");
		
		intentObj.Call<AndroidJavaObject>("putExtra",intentClass.GetStatic<string>("EXTRA_SUBJECT"), "Best Android Game");

		
		AndroidJavaClass unity = 
			new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity =
			unity.GetStatic<AndroidJavaObject>("currentActivity");
		//invoke android activity for passing inent to share data 
		currentActivity.Call("startActivity", intentObj);


	}

	private void IosSharing()
	{
		
	}
}
