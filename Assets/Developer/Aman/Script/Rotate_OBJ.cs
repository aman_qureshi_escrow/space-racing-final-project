﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate_OBJ : MonoBehaviour
{


	public bool Left = true;
	public bool right;

	public float Value = 0.1f;

	string baseCheck;
	
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (Time.timeScale == 1) {
			

			

			if (gameObject.layer == 8)
			{
				if (Left) {
					transform.Rotate (0, +Value, 0);
				} else if (right) {
					transform.Rotate (0, -Value, 0);
				}
			}
			else
			{
				if (gameObject.CompareTag("baseShip"))
				{
					if (Left) {
						transform.Rotate (0, +Value, 0);
					} else if (right) {
						transform.Rotate (0, -Value, 0);
					}
				}
				else
				{
					if (Left) {
						transform.Rotate (0, 0, +Value);
					} else if (right) {
						transform.Rotate (0, 0, -Value);
					}
				
				}
			}
			
		}

	}
}
