﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestruction : MonoBehaviour {

	private void Start()
	{
		StartCoroutine("DestroyMe");
	}

	IEnumerator DestroyMe()
	{
		yield return new WaitForSeconds(3f);
		Destroy(this.gameObject);
	}
}
