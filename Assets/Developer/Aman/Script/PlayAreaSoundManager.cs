﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAreaSoundManager : MonoBehaviour {

	public GameObject soundManager;

	public static PlayAreaSoundManager Instance = null;


	private void Awake()
	{
		GetSoundManager();

		if (Instance == null)
		{
			Instance = this;
		}
	}

	private void GetSoundManager()
	{
		soundManager = GameObject.Find("SoundManager");
		soundManager.GetComponent<SoundManager>().backGroundSound.mute = true;
		int randomSound = Random.Range(0, soundManager.GetComponent<SoundManager>().PlayBack.Length);
		soundManager.GetComponent<SoundManager>().PlayASound(randomSound);
	}

	private void OnDisable()
	{
//		soundManager.GetComponent<SoundManager>().backGroundSound.mute = false;
	}

}
