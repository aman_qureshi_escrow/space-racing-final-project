﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loading_Script : MonoBehaviour
{

	AsyncOperation async;
	public Image Fill_Bar;

	float counter = 0;
	public Text Count_TXT;

	public RawImage Load_RAW;
	public Texture[] Texture_IMG;
	public string SceneName;

	// Use this for initialization
	void Start ()
	{

		Time.timeScale = 1f;

		//====== Music ======
		if (PlayerPrefs.GetInt ("MUSIC") == 0) {
			AudioListener.volume = 1;

			//GameObject go = GameObject.Find ("SOUND");
//			if (go) {
//				go.GetComponent<Sound_Manager> ().BG_Play ();
//			}
		}
		//=================

//		if (PlayerPrefs.GetInt ("CHK_HOME") == 1) {
////			Application.LoadLevel ("Splash");
//			async = Application.LoadLevelAsync ("Splash");
//		} else {
//		Debug.Log(SceneName);
		if (PlayerPrefs.GetInt("Home") == 0)
		{
			SoundManager.Instance.PlayAreaBack.mute = true;
			SoundManager.Instance.backGroundSound.mute = false;
			async = SceneManager.LoadSceneAsync("splash");

		}
		else if(PlayerPrefs.GetInt("Home") == 1)
		{
			async = SceneManager.LoadSceneAsync(PlayerPrefs.GetString("CurrentScene"));
			SoundManager.Instance.PlayAreaBack.mute = false;
			SoundManager.Instance.backGroundSound.mute = true;
		}

//			Application.LoadLevel ("PlayArea_Final");
	//	}

		PlayerPrefs.SetInt ("CHK_HOME", 0);

		StartCoroutine (LoadNewScene ());
//
//		async = Application.LoadLevelAsync ("PlayArea_Final");
		async.allowSceneActivation = false;
	}

	// Update is called once per frame
	void Update ()
	{

		Count_TXT.text = "" + counter + "%";
//		Fill_Bar.fillAmount = counter / 100f;
	}

	IEnumerator LoadNewScene ()
	{
		yield return new WaitForSeconds (0.003f);
		counter++;

		if (counter >= 99) {
			async.allowSceneActivation = true;
			StopCoroutine (LoadNewScene ());
		} else {
			StartCoroutine (LoadNewScene ());
		}
	}

	IEnumerator LoadNewScene2 ()
	{
		yield return new WaitForSeconds (0.2f);
		counter++;

		Load_RAW.texture = Texture_IMG[(int)counter];

		if (counter >= 16) {
			async.allowSceneActivation = true;
			StopCoroutine (LoadNewScene2 ());
		} else {
			StartCoroutine (LoadNewScene2 ());
		}
	}

}
