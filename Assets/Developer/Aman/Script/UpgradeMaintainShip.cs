﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMaintainShip : MonoBehaviour
{

	public List<int> mainTainCrystal;
	public List<int> mainTainDiamonds;
	public List<TextMeshProUGUI> allUpgrades;
	public int defaultUpgradeValue;

	public Image DamageSlider;
	public TextMeshProUGUI damageText,finishNowText,mainTainPrice,timerTEXT;
	public GameObject currentShip, repairBt,MaintainBt;
	public static UpgradeMaintainShip Instance = null;
	public float tmp;
	public float timeLeft;
	public bool timestart;

	public string[] planetNames;
	public TextMeshProUGUI name, length;
	public GameObject newPlanetUnlocked;


	private void Awake()
	{
		//chek is new planet is unlocked ?
		for (int i = 0; i < planetNames.Length; i++)
		{
			if (PlayerPrefs.GetInt(planetNames[i]) == 10)
			{
				PlayerPrefs.SetInt(planetNames[i],0);
				newPlanetUnlocked.SetActive(true);
				//PlayerPrefs.SetInt(planetNames[i]+"unlocked", 0);
				string tempString = planetNames[i + 1].Substring(planetNames[i + 1].Length - 1);
				name.text = tempString;
				length.text = (i+1).ToString();


				if (planetNames[i].Equals(planetNames[0]))
				{
					PlayerPrefs.SetInt("Planet2", 10);
				}
				else if (planetNames[i].Equals(planetNames[1]))
				{
					PlayerPrefs.SetInt("Planet3", 10);
				}
				else if (planetNames[i].Equals(planetNames[2]))
				{
					PlayerPrefs.SetInt("Planet4", 10);
				}
				else if (planetNames[i].Equals(planetNames[3]))
				{
					PlayerPrefs.SetInt("Planet5", 10);
				}
			}
		}
	}

	public void newPlanetUnlockedOFF()
	{
		newPlanetUnlocked.SetActive(false);
	}


	// Use this for initialization
	void Start () {

		if (Instance == null)
		{
			Instance = this;
		}
		timestart = false;
	}

	public void DamageTextSet()
	{
//		Debug.Log("DamageTextSet");
		damageText.text = PlayerPrefs.GetInt("Damage" + currentShip.name).ToString() + "%";
//		Debug.Log(PlayerPrefs.GetInt("Damage"+currentShip.name));

		//	DamageSlider.fillAmount = PlayerPrefs.GetInt("Damage" + currentShip.name) / 100;
//		Debug.Log("Current Ship : " + currentShip.name);
		tmp = PlayerPrefs.GetInt("Damage" + currentShip.name);

		if (DamageSlider != null)
		{
			DamageSlider.fillAmount = tmp / 100f;
		}



		if (tmp > 75 && tmp < 100)
		{
			finishNowText.text = mainTainDiamonds[0].ToString();
			mainTainPrice.text = mainTainCrystal[0].ToString();
		}
		else if (tmp > 50 && tmp < 75)
		{
			finishNowText.text = mainTainDiamonds[1].ToString();
			mainTainPrice.text = mainTainCrystal[1].ToString();
		}
		else if (tmp > 25 && tmp < 50)
		{
			finishNowText.text = mainTainDiamonds[2].ToString();
			mainTainPrice.text = mainTainCrystal[2].ToString();
		}
		else if (tmp > 0 && tmp < 25)
		{
			finishNowText.text = mainTainDiamonds[3].ToString();
			mainTainPrice.text = mainTainCrystal[3].ToString();
		}

	}

	// Update is called once per frame
	void Update () {

		if (timestart == true)
		{
			timeLeft -= Time.deltaTime;
			timerTEXT.text = "TIME REQUIRED " + ConvertTimeToString(timeLeft);
			if (timeLeft <= 0)
			{
				SoundManager.Instance.BTSound(8);
				int maintainTemp = int.Parse(mainTainPrice.text);
				PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - maintainTemp);
				MaintainFinish();
			}
		}
	}

	public void MaintainFinish()
	{
		timestart = false;
		DamageSlider.fillAmount = 1f;
		SplashManager.Instance.DiamodAndCoinManage();
		PlayerPrefs.SetInt("Damage"+currentShip.name, 100);
		DamageTextSet();
	}

	public void FinishNow()
	{
		int maintainTemp = int.Parse(finishNowText.text);
		PlayerPrefs.SetInt("Daimonds", PlayerPrefs.GetInt("Daimonds") - maintainTemp);
		DamageSlider.fillAmount = 1f;
		SplashManager.Instance.DiamodAndCoinManage();
		PlayerPrefs.SetInt("Damage"+currentShip.name, 100);
		DamageTextSet();
	}

	string ConvertTimeToString(float time)
	{

		//Take the time and convert it into the number of minutes and seconds
		int minutes = (int)(time / 60);
		float seconds = time % 60f;

		//Create the string in the appropriate format for the time
		string output = minutes.ToString("00") + ":" + seconds.ToString("00");
		return output;
	}

	public void DefaultValueSet()
	{
		Debug.Log("Default set");

		for (int i = 0; i < SplashManager.Instance.spaceShips.Length; i++)
		{
			PlayerPrefs.SetInt("Ship-"+i+"-diamondSpeed",10);
			PlayerPrefs.SetInt("Ship-"+i+"-diamondHandling",10);
			PlayerPrefs.SetInt("Ship-"+i+"-diamondAcceleration",10);
			PlayerPrefs.SetInt("Ship-"+i+"-diamondArmor",10);
			PlayerPrefs.SetInt("Ship-"+i+"-diamondFireMachineGun",10);
		}

		for (int i = 0; i < allUpgrades.Count; i++)
		{
			allUpgrades[i].text = defaultUpgradeValue.ToString();
			Debug.Log(allUpgrades[i].text);
		}
		PlayerPrefs.SetInt("Starting", 10);
	}


	public void setDiamondValue(string value)
	{
		allUpgrades[0].text = PlayerPrefs.GetInt(value+"diamondSpeed").ToString();
		allUpgrades[1].text = PlayerPrefs.GetInt(value+"diamondHandling").ToString();
		allUpgrades[2].text = PlayerPrefs.GetInt(value+"diamondAcceleration").ToString();
		allUpgrades[3].text = PlayerPrefs.GetInt(value+"diamondArmor").ToString();
		allUpgrades[4].text = PlayerPrefs.GetInt(value+"diamondFireMachineGun").ToString();

	}

}
