﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScale : MonoBehaviour {

	// Use this for initialization
	[SerializeField]
	private float m_scrollDivider;
	public static float ScrollSpeed = 0.00001f;
	private Renderer m_renderer;
	private float m_offset = 0f;

	void Start()
	{
		m_renderer = GetComponent<Renderer>();
	}

	void Update()
	{
		m_offset -= Time.deltaTime * (ScrollSpeed / m_scrollDivider);

		m_renderer.material.mainTextureOffset = new Vector3(0, m_offset, 0);
	}
}
