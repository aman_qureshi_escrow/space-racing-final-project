﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class SoundManager : MonoBehaviour
{
	#region Audio Source
	[Header("Audio Source")]
	public AudioSource backGroundSound;
	public AudioSource buttonClick;
	public AudioSource shipSound;
	public AudioSource PlayAreaBack;
	#endregion

	#region List

	public List<int> RewardData = new List<int>();
	#endregion

	#region Audio Clips
	[Header("Audio Clips")]
	public AudioClip[] backgroundSounds;
	public AudioClip[] buttonSounds;
	public AudioClip[] shipSounds;
	public AudioClip[] PlayBack;
	#endregion

	#region IntegerValue
	private int backMusicRandom;
	#endregion

	#region GameInstance
	public static SoundManager Instance = null;
	#endregion

	#region Delgate
	public delegate void BackSoundListener(int i);
	public static event BackSoundListener BacksoundListener;
	public delegate void ButtonSoundListener(int i);
	public static event ButtonSoundListener ButtonsoundListener;
	public delegate void ShipSoundListener(int i);
	public static event ShipSoundListener ShipsoundListener;
	public delegate void PlayAreaSoundListener(int i);
	public static event PlayAreaSoundListener playAreaSoundListener;

	#endregion

	#region MonoBehaviour
	private void OnEnable()
	{
		BacksoundListener += BackPlay;
		ButtonsoundListener += ButtonPlay;
		ShipsoundListener += ShipPlay;
		playAreaSoundListener += PlayArBack;

		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else if (Instance != null)
		{
			Destroy(gameObject);
		}
	}

	void Awake()
	{


	}

	private void OnDisable()
	{
		BacksoundListener -= BackPlay;
		ButtonsoundListener -= ButtonPlay;
		ShipsoundListener -= ShipPlay;
		playAreaSoundListener -= PlayArBack;
	}

	private void Start()
	{
		backMusicRandom = UnityEngine.Random.Range(0, 2);
		BackPlay(backMusicRandom);
	}

	#endregion


	#region PrivateMethods

	private void BackPlay(int soundId)
	{
		backGroundSound.clip = backgroundSounds[soundId];
		backGroundSound.loop = true;
		backGroundSound.Play();
	}

	private void ButtonPlay(int soundId)
	{
		buttonClick.clip = buttonSounds[soundId];
		buttonClick.Play();
	}

	private void ShipPlay(int SoundId)
	{
		shipSound.clip = shipSounds[SoundId];
		shipSound.Play();
	}

	private void PlayArBack(int SoundId)
	{
//		Debug.Log(SoundId);
		PlayAreaBack.clip = PlayBack[SoundId];
		PlayAreaBack.Play();
	}

	#endregion



	#region PublicMethods

	public void BKSound(int soundID)
	{
		BacksoundListener(soundID);
	}

	public void BTSound(int soundID)
	{
		ButtonsoundListener(soundID);
	}

	public void SPSound(int soundID)
	{
		ShipsoundListener(soundID);
	}

	public void PlayASound(int soundID)
	{
//		Debug.Log(soundID);
		PlayArBack(soundID);
	}




	#endregion



}
