﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SWS;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;

public class RankManager : MonoBehaviour
{

	public static RankManager Instance;
	public TextMeshProUGUI[] textRanks;

	private Dictionary<string, Player> players;
	private Dictionary<string, Player> sortedPlayers;
	
	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	private void Start()
	{
		players = new Dictionary<string, Player>();
	}

	public void SetRank(Player player)
	{
		players[player.name] = player;
		IOrderedEnumerable<KeyValuePair<string, Player>> sortedPlayer =
			players.OrderBy(x => x.Value.distanceToWayPoint).OrderByDescending(x => x.Value.activeWayPointIndex);
		int i = 0;
		foreach (KeyValuePair<string, Player> item in sortedPlayer)
		{
			textRanks[i].text = (i + 1) + " . " + item.Value.name;
		}
	}
	
	
	
	//cmpare car Position 1st 2nd 3 and 4th
	public class progressionCompare : IComparable<progressionCompare>
	{
		public splineMove ship;
		public int total;

		public progressionCompare(splineMove newShip, int newTotal)
		{
			ship = newShip;
			total = newTotal;
		}
		
		public int CompareTo(progressionCompare other)
		{
			if (other == null)
			{
				return 1;
			}

			return total - other.total;
		}
	
	}
	
}
