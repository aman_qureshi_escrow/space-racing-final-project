﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Image_Open_Close : MonoBehaviour {


	public bool RAW_IMG = true;
	public bool TXT = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		float x = Mathf.PingPong(Time.time * 1f, 1.0f);

//		float x = Mathf.PingPong(Time.deltaTime* 1f, 1.0f);

		if (RAW_IMG) {
			GetComponent<RawImage> ().color = new Color (255, 255, 255, x); // if color change
		} else if (TXT) {
			GetComponent<Text> ().color = new Color (255, 0, 0, x); // if color change
		}

//		GetComponent<RawImage> ().color = new Color (1, 1, 1, x);  // No change color
	}
}
