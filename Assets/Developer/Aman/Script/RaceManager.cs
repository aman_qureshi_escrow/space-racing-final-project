﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SWS;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RaceManager : MonoBehaviour
{

	public Ship[] allShips;
	public Ship[] shipOrder;
	public TextMeshProUGUI[] textRanks;
	public TextMeshProUGUI[] time;
	public PathManager path;
	private int cnt_Variable;
	public Transform lastIndex;
	public static RaceManager Instance;
	public List<GameObject> enemyList = new List<GameObject>();
	public Dictionary<string, float> timeStore = new Dictionary<string, float>();
	public Dictionary<string, float> timeStore1 = new Dictionary<string, float>();
	public List<TextMeshProUGUI> rewards;
	public GameObject soundManager;
	bool showRank = true;

	#region Venus

	public float timeLeft;
	[SerializeField]
	public List<GameObject> allshipVenus1;
	#endregion

	private void Awake()
	{
		soundManager = FindObjectOfType<SoundManager>().gameObject;

		if (Instance == null)
		{
			Instance = this;
		}
		for (int i = 0; i < path.waypoints.Length; i++)
		{
			path.waypoints[i].transform.gameObject.GetComponent<WayPointValue>().myOwnValue = i;
		}

		if (SceneManager.GetActiveScene().name == "Venus1" || SceneManager.GetActiveScene().name == "Mars1")
		{
			GetComponent<GameManager>().shipUI.gameObject.SetActive(false);
		}

		lastIndex = path.waypoints.Last();
	}


	// Use this for initialization
	void Start () {
		//set up the new ship object
		shipOrder = new Ship[allShips.Length];

		InvokeRepeating("ManualUpdate", 0.5f,0.5f);


		for (int i = 0; i < allShips.Length; i++)
		{
			if (allShips[i].gameObject.CompareTag("Enemy"))
			{
				enemyList.Add(allShips[i].gameObject);
				allshipVenus1.Add(allShips[i].gameObject);
			}
			if(allShips[i].gameObject.CompareTag("Player") && allShips[i].isActiveAndEnabled)
			{
				allshipVenus1.Add(allShips[i].gameObject);
			}
		//	Debug.Log(enemyList[i].gameObject.name);
		}

		InvokeRepeating("ScoreBoardData", 1f, 1f);
		InvokeRepeating("test",0.1f,0.1f);
	//	InvokeRepeating("VenusTimer", 1f, 60f);
		if (SceneManager.GetActiveScene().name.Contains("1"))
		{
			rewards[0].text = soundManager.GetComponent<SoundManager>().RewardData[3].ToString();
			rewards[1].text = soundManager.GetComponent<SoundManager>().RewardData[4].ToString();
			rewards[2].text = soundManager.GetComponent<SoundManager>().RewardData[5].ToString();
			rewards[3].text = "3";
			rewards[4].text = "1";
		}
		else
		{
			rewards[0].text = soundManager.GetComponent<SoundManager>().RewardData[0].ToString();
			rewards[1].text = soundManager.GetComponent<SoundManager>().RewardData[1].ToString();
			rewards[2].text = soundManager.GetComponent<SoundManager>().RewardData[2].ToString();
			rewards[3].text = "3";
			rewards[4].text = "1";
		}
	}


	void ScoreBoardData()
	{
		timeStore.Clear();
		timeStore1.Clear();

		if (GetComponent<GameManager>().vehicleMovement != null)
		{
			timeStore.Add(GetComponent<GameManager>().vehicleMovement.name, LapTimeUI.Instance.playerfinalTime);
		}

		for (int i = 0; i < enemyList.Count; i++)
		{
			if (enemyList[i].gameObject.activeSelf == true)
			{
				timeStore.Add(enemyList[i].name,enemyList[i].GetComponent<AiController>().finalTimeFloat);
			}
		}



		foreach (KeyValuePair<string, float> ssj in
			timeStore.OrderBy(key => key.Value))
		{
			//	Debug.Log(ssj.Key + " " + ConvertTimeToString(ssj.Value).ToString());
//			Debug.Log(ssj.Key);
			timeStore1.Add(ssj.Key, ssj.Value);
		}
	}

	/*void VenusTimer()
	{}*/

	private void Update()
	{
		// for venus
		if (SceneManager.GetActiveScene().name == "Venus" || SceneManager.GetActiveScene().name == "Earth" || SceneManager.GetActiveScene().name == "Jupiter2")
		{
			if (GameManager.instance.isGameOver == false)
			{
				timeLeft -= Time.deltaTime;
			}

			LapTimeUI.Instance.timer.text = ConvertTimeToString(timeLeft).ToString();
			if (timeLeft < 0)
			{
				if (GameManager.instance.isGameOver == true)
				{
					Time.timeScale = 0f;
					GameManager.instance.Venus1Eliminate();
				}
				else
				{
					GameManager.instance.RunOutOfTime();
				}
			}
		}
	}


	public void test()
	{

		if (GameManager.instance.isGameOver == true)
		{
			Time.timeScale = 10;
		}

		var target = timeStore1.ToList();
		for (int i = 0; i < target.Count; i++)
		{

			if (GameManager.instance.gameOverUI.activeSelf == true && target[i].Key.Contains("Ship"))
			{

//				Debug.Log(target[i].Key + target[i].Value);

				if (showRank == true)
				{
					showRank = false;
//					Debug.Log(i);
					GameManager.instance.gameOverUI.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text =  LapTimeUI.Instance.finalTimeLabel.text;
					GameManager.instance.gameOverUI.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
				}
			}
			if (target[i].Key.Contains("Ship"))
			{
				textRanks[i].text = target[i].Key;
				time[i].text = ConvertTimeToString(target[i].Value).ToString();
			}
			else if (target[i].Key.Contains("Enemy"))
			{
				textRanks[i].text = target[i].Key;
				time[i].text = ConvertTimeToString(target[i].Value).ToString();
			}

		}

	}


	public void ManualUpdate()
	{
		foreach (Ship pos in allShips)
		{
			if (pos.gameObject == null)
			{
				return;
			}
			else
			{
				shipOrder[pos.GetShipPosition(allShips) - 1] = pos;
			}
		}
	}


	private string ConvertTimeToString(float lapTime)
	{
		//takes time and convert it into the number of minutes and seconds
		int minutes = (int) (lapTime / 60);
		float seconds = lapTime % 60f;

		//create the string in the appropriae format for the time
		string output = minutes.ToString("00") + ":" + seconds.ToString("00.000");
		return output;
	}

}
