﻿using System.Collections;
using SWS;
using UnityEngine;

public class Ship : MonoBehaviour
{

	public int currentWayPoint;
	public int currentLap;
	public Transform lastWayPoint;
	public int nbWaypoint;// set amout of waypoints

	private static int WAYPOINT_VALUE = 100;
	private static int LAP_VALUE = 10000;
	private static int CPT_WAYPOINT = 0;
	private MyPlayers myplayer;
	private int Delay;

	public int triggerValue = 1;
	
	enum MyPlayers
	{
		Player,Enemy
	}
	
	//USER FOR INITIALIZE
	public void Initialize()
	{
		currentWayPoint = 0;
		currentLap = 0;
		CPT_WAYPOINT = 0;
	}


	public void TriggerValueChange()
	{
		StartCoroutine("TriggerValueChangeToOriginal");
	}

	IEnumerator TriggerValueChangeToOriginal()
	{
		yield return new WaitForSeconds(10f);
		triggerValue = 1;
	}
	
	
	private void Awake()
	{
		if (this.gameObject.CompareTag("Player"))
		{
			myplayer = MyPlayers.Player;
		}
		else
		{
			myplayer = MyPlayers.Enemy;
		}
		CheckLevel(PlayerPrefs.GetString("Level"));
		Delay = 0;
	}

	private void CheckLevel(string getString)
	{
		if (getString.Contains("Level1"))
		{
			
		}
		else
		{
			//GetComponent<splineMove>().pathContainer.
		}
	}

	private void OnTriggerEnter(Collider other)
	{
//		Debug.Log(other.gameObject.tag);

		
		if(other.gameObject.CompareTag("Untagged") && other.gameObject.GetComponent<WayPointValue>() != null)
		{
			//gameObject.GetComponent<WayPointValue>() != null;
//			Debug.Log(other.gameObject.tag);
			//	string otherTag = other.gameObject.tag;
			//	currentWayPoint = int.Parse(otherTag);
			
			currentWayPoint = other.gameObject.GetComponent<WayPointValue>().myOwnValue;
		
//			if (currentWayPoint == 1 && CPT_WAYPOINT == nbWaypoint)
//			{
//				//complete a lap to increase current lap
//				currentLap++;
//				CPT_WAYPOINT = 0;
//			}

			if (CPT_WAYPOINT == currentWayPoint - 1)
			{
			//	lastWayPoint = other.transform;
				CPT_WAYPOINT++;
			}

			if (other.gameObject.transform == RaceManager.Instance.lastIndex &&  myplayer == MyPlayers.Player && Delay == 0)
			{
				if (currentLap < GameManager.instance.numberOfLaps)
				{
					Delay = 1;
					currentLap++;
					StartCoroutine(DelayValueChange());
				}
				else
				{
					GameManager.instance.PlayerCompletedLap();
				}
				
			}
			
			if (other.gameObject.transform == RaceManager.Instance.lastIndex &&  myplayer == MyPlayers.Enemy && Delay == 0)
			{
				if (currentLap < GameManager.instance.numberOfLaps)
				{
					Delay = 1;
					currentLap++;
					StartCoroutine(DelayValueChange());
				}

			}

			
		}
	}


	IEnumerator DelayValueChange()
	{
		yield return new WaitForSeconds(3f);
		Delay = 0;
	}
	
	public float GetDistance()
	{

		if (!gameObject.active)
		{
			return 0f;
		}
		return (transform.position - lastWayPoint.position).magnitude
			       + currentWayPoint * WAYPOINT_VALUE
			       + currentLap * LAP_VALUE;
		
	}

	public int GetShipPosition(Ship[] allShip)
	{
		float distance = GetDistance();
		int position = 1;
		foreach (Ship ship in allShip)
		{
			if (ship.GetDistance() > distance)
			{
				position++;
			}

		}
	//	Debug.Log( this.gameObject.name +  position);
		return position;
	}

}
